package ch.androidapp.hevs.fulltopia.Modules.Account;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class AccountSetupPhotoFragment extends Fragment implements Step {

    private static final int RESULT_SELECT_IMG = 1;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference referenceCurrentUser;
    private StorageReference referenceProfilePictures;

    private CircleImageView profilePictureView;
    private Button buttonChangePicture;
    private Button buttonChangeStatus;
    private TextView labelDisplayName;
    private TextView labelStatus;
    private ProgressDialog progressDialog;

    private AlertDialog.Builder alertBuilder;
    private EditText statusMessageInput;
    private String statusMessageValue;


    private void selectImageFromGallary()
    {
        /* Create intent to Open Image */
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), RESULT_SELECT_IMG);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate( R.layout.account_setup_photo_fragment, container, false);
        float dpi = getContext().getResources().getDisplayMetrics().density;
        profilePictureView = (CircleImageView) v.findViewById( R.id.profile_picture );
        buttonChangePicture = (Button) v.findViewById( R.id.stepper_profile_change_picture );
        buttonChangeStatus = (Button) v.findViewById( R.id.stepper_profile_change_status );
        labelDisplayName = (TextView) v.findViewById( R.id.stepper_profile_label_title );
        labelStatus = (TextView) v.findViewById( R.id.stepper_profile_label_status );


        progressDialog = new ProgressDialog( getContext() );
        alertBuilder = new AlertDialog.Builder(getContext());
        alertBuilder.setTitle(R.string.stepper_profile_change_status_title);
        alertBuilder.setMessage(R.string.stepper_profile_change_status_message);

        // Set an EditText view to get user input
        statusMessageInput = new EditText(getContext());
        //noinspection deprecation
        alertBuilder.setView(statusMessageInput, (int)(19*dpi), (int)(5*dpi), (int)(14*dpi), (int)(5*dpi) );
        firebaseAuth = FirebaseAuth.getInstance();
        referenceProfilePictures = FirebaseStorage.getInstance().getReference().child("pictures").child("profiles");
        currentUser = firebaseAuth.getCurrentUser();
        String currentUserId = currentUser.getUid();

        referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child("users").child( currentUserId );
        referenceCurrentUser.getRef().addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                labelDisplayName.setText(dataSnapshot.child("name").getValue(String.class));
                statusMessageValue = dataSnapshot.child("status").getValue(String.class);
                statusMessageInput.setText(statusMessageValue);
                labelStatus.setText(statusMessageValue);

                String picturePath = dataSnapshot.child("profile_picture").getValue(String.class);
                if(!picturePath.equals("default")){
                    Picasso.with(getContext()).load(picturePath).placeholder(R.drawable.no_picture).into(profilePictureView);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        alertBuilder.setPositiveButton(R.string.stepper_profile_change_status_btn_validate, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                progressDialog = new ProgressDialog( getContext() );
                progressDialog.setTitle( "Updating status" );
                progressDialog.setMessage( "Please wait a few seconds..." );
                progressDialog.setCanceledOnTouchOutside( false );
                progressDialog.show();

                String newStatusMessageValue = statusMessageInput.getText().toString();
                referenceCurrentUser.child("status").setValue(newStatusMessageValue).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            progressDialog.dismiss();
                        }else{
                            Log.w( "FULLTOPIA EXEPTION", "The status message update failed.", task.getException() );
                            progressDialog.dismiss();
                        }
                    }
                });
            }
        });


        alertBuilder.setNegativeButton(R.string.stepper_profile_change_status_btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });
        final AlertDialog alert = alertBuilder.create();


        buttonChangePicture.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                selectImageFromGallary();
            }
        } );

        buttonChangeStatus.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                alert.show();
            }
        } );



        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("APP_DEBUG", String.valueOf(requestCode));

        try {
            // When an Image is picked
            if (requestCode == RESULT_SELECT_IMG && resultCode == Activity.RESULT_OK
                    && null != data) {
                Uri selectedImage = data.getData();

                CropImage.activity(selectedImage)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(15,10)
                        .setMinCropWindowSize(300,200)
                        .start(getActivity(), this);
            }
            // when image is cropped
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Log.d("APP_DEBUG",result.toString());
                if (resultCode == Activity.RESULT_OK) {
                    Uri resultUri = result.getUri();

                    progressDialog = new ProgressDialog( getContext() );
                    progressDialog.setTitle( "Uploading image" );
                    progressDialog.setMessage( "Please wait a few seconds..." );
                    progressDialog.setCanceledOnTouchOutside( false );
                    progressDialog.show();


                    Bitmap compressedImageBitmap = new Compressor(getContext())
                            .setMaxWidth(150)
                            .setMaxHeight(100)
                            .setQuality(60)
                            .compressToBitmap( new File( resultUri.getPath() ) );


                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    compressedImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    final byte[] thumbImg = baos.toByteArray();


                    StorageReference imagePath = referenceProfilePictures.child( currentUser.getUid()+".jpg");
                    final StorageReference thumbImagePath = referenceProfilePictures.child("thumbs").child( currentUser.getUid()+".jpg");


                    imagePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull final Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful()){

                                @SuppressWarnings("VisibleForTests")
                                final String downloadUrl = task.getResult().getDownloadUrl().toString();

                                UploadTask uploadTask = thumbImagePath.putBytes(thumbImg);
                                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumbTask) {
                                        if(task.isSuccessful()){

                                            @SuppressWarnings("VisibleForTests")
                                            final String thumbDownloadUrl = thumbTask.getResult().getDownloadUrl().toString();

                                            Map upadtePicturesMap = new HashMap<String, String>();
                                            upadtePicturesMap.put("profile_picture", downloadUrl);
                                            upadtePicturesMap.put("profile_picture_thumbnail", thumbDownloadUrl);


                                            referenceCurrentUser.updateChildren(upadtePicturesMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        progressDialog.dismiss();
                                                    }else{
                                                        Log.w( "FULLTOPIA EXEPTION", "The status message update failed.", task.getException() );
                                                        progressDialog.dismiss();
                                                    }
                                                }
                                            });

                                            progressDialog.dismiss();

                                        }else{
                                            Log.w( "FULLTOPIA EXEPTION", "The image thumbnail  upload failed.", task.getException() );
                                            progressDialog.dismiss();

                                        }
                                    }
                                });
                            }else{
                                Log.w( "FULLTOPIA EXEPTION", "The image upload failed.", task.getException() );
                                progressDialog.dismiss();
                            }
                        }
                    });


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
            else {
                Toast.makeText(getActivity(), "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong"+e.getMessage(), Toast.LENGTH_LONG)
                    .show();
        }

    }


}