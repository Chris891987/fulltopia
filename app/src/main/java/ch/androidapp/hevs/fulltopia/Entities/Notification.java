package ch.androidapp.hevs.fulltopia.Entities;

import com.google.firebase.database.Exclude;

public class Notification {
    private String title;
    private String body;
    private String icon = "default";
    private String clickAction;
    private String notificationType;
    private String userReceiverKey;
    private Boolean seen = false;

    @Exclude
    private String userSender;

    public Notification() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody( String body ) {
        this.body = body;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon( String icon ) {
        this.icon = icon;
    }

    public String getClickAction() {
        return clickAction;
    }

    public void setClickAction( String clickAction ) {
        this.clickAction = clickAction;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType( String notificationType ) {
        this.notificationType = notificationType;
    }

    public String getUserReceiverKey() {
        return userReceiverKey;
    }

    public void setUserReceiverKey( String userReceiverKey ) {
        this.userReceiverKey = userReceiverKey;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen( Boolean seen ) {
        this.seen = seen;
    }

    public String getUserSender() {
        return userSender;
    }

    public void setUserSender( String userSender ) {
        this.userSender = userSender;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", icon='" + icon + '\'' +
                ", clickAction='" + clickAction + '\'' +
                ", notificationType='" + notificationType + '\'' +
                ", userReceiverKey='" + userReceiverKey + '\'' +
                ", seen=" + seen +
                ", userSender='" + userSender + '\'' +
                '}';
    }
}
