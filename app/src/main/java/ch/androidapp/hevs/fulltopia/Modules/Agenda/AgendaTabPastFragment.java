package ch.androidapp.hevs.fulltopia.Modules.Agenda;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewAgendaAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.R;

import java.util.ArrayList;
import java.util.List;


public class AgendaTabPastFragment extends Fragment {

    private FirebaseUser currentUser;
    private DatabaseReference currentUserKey;
    private DatabaseReference eventsByUser;


    private TextView eventName;

    private List<Event> postsEvent = new ArrayList<Event>();

    private long maxState;
    private long currentState;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        final View v = inflater.inflate( R.layout.agenda_fragment_tab_past, container, false );
        eventName = (TextView) v.findViewById( R.id.eventName);

        final long timestamp = System.currentTimeMillis();

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        this.currentUserKey = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
        this.eventsByUser = FirebaseDatabase.getInstance().getReference().child("users_events").child(currentUser.getUid());
        this.eventsByUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                maxState = dataSnapshot.getChildrenCount();
                currentState = 0;
                for (DataSnapshot e : dataSnapshot.getChildren()) {

                    final Long l = e.child("time").getValue(Long.class);

                    long timeDiff = timestamp - l;
                    if (timeDiff > 0) {

                        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("events");
                        ref.child(e.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot data) {
                                Event e = new Event();
                                e.setId(data.getKey());

                                e.setName(data.child("name").getValue(String.class));
                                e.setStartDate(data.child("startDate").getValue(String.class));
                                postsEvent.add(e);

                                currentState++;
                                printResults(v);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                    else {
                        maxState--;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;

    }



    public void onClick(View v)
    {
        switch(v.getId()){
            case R.id.moreButton:
                Toast toast = new Toast(getActivity());
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(v);
                toast.setText("test");
                toast.show();
                break;
        }
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        return;
    }

    private void printResults(View v) {
        if (currentState == maxState) {
            RecyclerView agendaView = (RecyclerView) v.findViewById(R.id.recycler_view_list_agenda);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };

            agendaView.setLayoutManager(linearLayoutManager);
            RecyclerViewAgendaAdapter eventAdapter = new RecyclerViewAgendaAdapter(getActivity().getLayoutInflater(), postsEvent);
            agendaView.setHasFixedSize(true);
            agendaView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            agendaView.setAdapter(eventAdapter);
        }
    }

}
