package ch.androidapp.hevs.fulltopia.Modules.Message;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewAgendaAdapter;
import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewNotificationsAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Entities.Notification;
import ch.androidapp.hevs.fulltopia.Entities.User;
import ch.androidapp.hevs.fulltopia.R;

import java.util.ArrayList;


public class MessageTabNotificationsFragment extends Fragment {

    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference users;
    private ArrayList<Notification> userNotifications;
    private RecyclerView notificationView;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View v = inflater.inflate( R.layout.message_fragment_tab_notifications, container, false );

        //Reference to database
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();

        users = FirebaseDatabase.getInstance().getReference().child( "notifications" );
        userNotifications = new ArrayList<Notification>();

        final RecyclerView notificationView = (RecyclerView) v.findViewById( R.id.recycler_view_notifications );
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager( getActivity() ) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        FirebaseDatabase.getInstance().getReference().child( "notifications" ).child( currentUser.getUid() ).orderByChild( "seen" ).equalTo( false ).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {
                        Log.w("TIAGO", dataSnapshot.toString());
                        for ( DataSnapshot currentNotificationData : dataSnapshot.getChildren() ) {
                            Notification notification = new Notification();
                            notification.setTitle( currentNotificationData.child("title").getValue(String.class) );
                            notification.setBody( currentNotificationData.child("body").getValue(String.class) );
                            notification.setIcon( currentNotificationData.child("icon").getValue(String.class) );
                            notification.setNotificationType( currentNotificationData.child("notificationType").getValue(String.class) );
                            userNotifications.add( notification );
                        }

                        notificationView.setLayoutManager( linearLayoutManager );
                        RecyclerViewNotificationsAdapter notificationAdapter = new RecyclerViewNotificationsAdapter( getActivity().getLayoutInflater(), userNotifications );
                        notificationView.setHasFixedSize( true );
                        notificationView.setLayoutManager( new LinearLayoutManager( getContext(), LinearLayoutManager.VERTICAL, false ) );
                        notificationView.setAdapter( notificationAdapter );

                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {

                    }
                }
            );




        return v;
    }


    @Override
    public void onActivityCreated( Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );
        setHasOptionsMenu( true );
    }


    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
        menu.clear();
        return;
    }

}
