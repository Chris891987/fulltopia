package ch.androidapp.hevs.fulltopia.Modules.Account;



import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import ch.androidapp.hevs.fulltopia.Modules.Global.GPSTracker;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.HashMap;
import java.util.Map;
/**
 * Class for setup location
 */
public class AccountSetupLocalisationFragment extends Fragment implements Step {

    private static final String TAG = "FULLTOPIA_EXEPTION";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference referenceCurrentUser;

    private Button btnOpenSearcher;
    private PlaceAutocompleteFragment autocompleteFragment = null;
    private LatLng newAddress;
    private LatLng phonePosition;

    MapView mapView;
    GoogleMap googleMap;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        final View rootView = inflater.inflate( R.layout.account_setup_localisation_fragment, container, false );

        //set mapview
        mapView = (MapView) rootView.findViewById( R.id.mapview );
        mapView.onCreate( savedInstanceState );
        mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize( getActivity().getApplicationContext() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }


        //connection to firebase
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child( "users" ).child( currentUser.getUid() );

        mapView.getMapAsync( new OnMapReadyCallback() {
            @Override
            public void onMapReady( GoogleMap mMap ) {
                googleMap = mMap;
            }
        } );

        referenceCurrentUser.getRef().addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {


                if ( dataSnapshot.hasChild("locationLongitude") && dataSnapshot.hasChild("locationLatitude") ) {
                    // For dropping a marker at a point on the Map

                    phonePosition = new LatLng(0,0 );

                    googleMap.addMarker( new MarkerOptions().position( phonePosition ).title( "I'm Here" ).snippet( "This is my address" ) );

                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target( phonePosition ).zoom( 12 ).build();
                    googleMap.animateCamera( CameraUpdateFactory.newCameraPosition( cameraPosition ) );
                } else {
                    //get location with gps
                    GPSTracker gps = new GPSTracker( getContext() );
                    if ( gps.canGetLocation() ) {
                        phonePosition = new LatLng( gps.getLatitude(), gps.getLongitude() );
                    } else {
                        gps.showSettingsAlert();
                        phonePosition = new LatLng( 0, 0 );
                    }

                    //set my position
                    Map position = new HashMap<>();
                    position.put( "locationLatitude", String.valueOf( phonePosition.latitude ) );
                    position.put( "locationLongitude", String.valueOf( phonePosition.longitude ) );

                    //update location
                    referenceCurrentUser.updateChildren( position ).addOnCompleteListener( new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete( @NonNull Task<Void> task ) {
                            if ( task.isSuccessful() ) {
                                Log.w( "FULLTOPIA EXEPTION", " SAVE SUCCESS." );
                            } else {
                                Log.w( "FULLTOPIA EXEPTION", " SAVE FAIL.", task.getException() );
                            }
                        }
                    } );
                }
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        } );


        if ( autocompleteFragment == null ) {
            autocompleteFragment = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById( R.id.place_autocomplete_fragment );
        }


        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter( AutocompleteFilter.TYPE_FILTER_CITIES )
                .build();
        autocompleteFragment.setFilter( typeFilter );

        autocompleteFragment.setOnPlaceSelectedListener( new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected( Place place ) {
                // TODO: Get info about the selected place.
                Log.i( TAG, "Place: " + place.getName() );//get place details here


                // For dropping a marker at a point on the Map
                newAddress = place.getLatLng();

                Map position = new HashMap<>();
                position.put( "locationLatitude", String.valueOf( newAddress.latitude ) );
                position.put( "locationLongitude", String.valueOf( newAddress.longitude ) );
                position.put( "locationName", String.valueOf( place.getName() ) );

                referenceCurrentUser.updateChildren( position ).addOnCompleteListener( new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( @NonNull Task<Void> task ) {
                        if ( task.isSuccessful() ) {
                            googleMap.addMarker( new MarkerOptions().position( newAddress ).title( "I'm Here" ).snippet( "This is my address" ) );

                            // For zooming automatically to the location of the marker
                            CameraPosition cameraPosition = new CameraPosition.Builder().target( newAddress ).zoom( 14 ).build();
                            googleMap.animateCamera( CameraUpdateFactory.newCameraPosition( cameraPosition ) );
                        } else {
                            Log.w( "FULLTOPIA EXEPTION", " SAVE FAIL.", task.getException() );
                        }
                    }
                } );


            }

            @Override
            public void onError( Status status ) {
                Log.i( TAG, "ERROR CLOSE" );
            }

        } );

        //research a location with the city
        btnOpenSearcher = (Button) rootView.findViewById( R.id.btn_show_searcher );
        btnOpenSearcher.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                LinearLayout searcherLayout = (LinearLayout) rootView.findViewById( R.id.search_layout );
                //searcherLayout.setVisibility(View.VISIBLE);
                EditText searcher = (EditText) autocompleteFragment.getView().findViewById( R.id.place_autocomplete_search_input );

                searcher.callOnClick();

            }
        } );


        return rootView;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError( @NonNull VerificationError error ) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

}