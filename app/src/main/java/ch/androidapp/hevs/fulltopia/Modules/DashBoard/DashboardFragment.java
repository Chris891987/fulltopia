package ch.androidapp.hevs.fulltopia.Modules.DashBoard;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ch.androidapp.hevs.fulltopia.R;


public class DashboardFragment extends Fragment {
    private static DashboardFragment fragment = null;

    public static DashboardFragment newInstance() {
        fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }

    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View myInflatedView = inflater.inflate(R.layout.dashboard_fragment, container, false);
        TabLayout tabLayout = ( TabLayout ) myInflatedView.findViewById( R.id.tab_layout_dashboard);
        tabLayout.addTab( tabLayout.newTab().setText( R.string.event ) );
        tabLayout.addTab( tabLayout.newTab().setText( R.string.commuties ) );
        tabLayout.setTabGravity( TabLayout.GRAVITY_FILL );


        final ViewPager viewPager = ( ViewPager ) myInflatedView.findViewById( R.id.pager_dashboard);
        final PagerAdapterDashboard adapter = new PagerAdapterDashboard( getFragmentManager(), tabLayout.getTabCount() );
        viewPager.setAdapter( adapter );
        viewPager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener(tabLayout) );
        tabLayout.addOnTabSelectedListener( new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected( TabLayout.Tab tab ) {
                viewPager.setCurrentItem( tab.getPosition() );
            }

            @Override
            public void onTabUnselected( TabLayout.Tab tab ) {

            }

            @Override
            public void onTabReselected( TabLayout.Tab tab ) {

            }
        });
        return myInflatedView;
    }
}
