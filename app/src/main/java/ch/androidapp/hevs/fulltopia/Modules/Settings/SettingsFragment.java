package ch.androidapp.hevs.fulltopia.Modules.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import ch.androidapp.hevs.fulltopia.Modules.Account.AccountLoginActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Fragment for each preference
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference database;
    private String currentUser;
    DatabaseReference currentUserReference;
    Map updateMessagePermission = new HashMap<String, String>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //link to the preferences xml values
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);

        //firebase connection
        firebaseAuth = FirebaseAuth.getInstance();
        currentUserReference = FirebaseDatabase.getInstance().getReference().child("users").child( FirebaseAuth.getInstance().getCurrentUser().getUid() );


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        if (v != null) {
            ListView lv = (ListView) v.findViewById(android.R.id.list);
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) lv.getLayoutParams();
            layoutParams.setMargins(0, 280, 0, 0);
            lv.setPadding(10, 10, 10, 10);
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(FulltopiaUtils.KEY_PREF_LANGUAGE) || key.equals(FulltopiaUtils.KEY_PREF_THEME) || key.equals(FulltopiaUtils.KEY_PREF_NOTIFICATION)) {


            //Retrieve Selected Data, here 1 ou 2
            String value = sharedPreferences.getString(key, "");

            int valueInt = Integer.parseInt(value);

            //key of preference selected
            String preference = key.toString();


            //NOTIFICATION
            if (FulltopiaUtils.KEY_PREF_NOTIFICATION.equals(preference)) {

                //ALLOW
                if (valueInt == 2) {
                    //if == 1 ALLOW
                    //if == 2 NOT ALLOW

                    updateMessagePermission.put("notification_allowed", "false");
                    currentUserReference.updateChildren( updateMessagePermission ).addOnCompleteListener( new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete( @NonNull Task<Void> task ) {

                        }
                    } );

                }
                else{

                    //so, allowed
                    updateMessagePermission.put("notification_allowed", "true");
                    currentUserReference.updateChildren( updateMessagePermission ).addOnCompleteListener( new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete( @NonNull Task<Void> task ) {
                        }
                    } );

                }
                getActivity().recreate();
            }
        }
    }
}