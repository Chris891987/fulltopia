package ch.androidapp.hevs.fulltopia.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ch.androidapp.hevs.fulltopia.Entities.Notification;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewNotificationsAdapter extends RecyclerView.Adapter<RecyclerViewNotificationsAdapter.PostsViewHolder> {
    private LayoutInflater inflater;
    private List<Notification> notifications;

    public RecyclerViewNotificationsAdapter(LayoutInflater inflater, List<Notification> notifications) {
        this.inflater = inflater;
        this.notifications = notifications;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemPost = inflater.inflate(R.layout.list_messages, parent, false);
        PostsViewHolder postsViewHolder = new PostsViewHolder( itemPost);
        postsViewHolder.view = itemPost;
        postsViewHolder.userName = (TextView) itemPost.findViewById(R.id.chat_single_message);
        postsViewHolder.messageDate = (TextView) itemPost.findViewById(R.id.messageDate);
        postsViewHolder.messageText = (TextView) itemPost.findViewById(R.id.messageText);
        postsViewHolder.profilePicture = (CircleImageView) itemPost.findViewById(R.id.chat_single_icon);


        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(final PostsViewHolder holder, final int position) {
        final Notification notification  = notifications.get(position);
        holder.userName.setText(notification.getTitle());
        holder.messageText.setText(notification.getBody());


        if(!notification.getIcon().equals("default")){
            Picasso.with( this.inflater.getContext()).load( notification.getIcon() ).placeholder(R.drawable.no_profile_picture).into( holder.profilePicture );
        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public static class PostsViewHolder extends RecyclerView.ViewHolder {
        View view;

        private Context context;
        TextView userName, messageDate,messageText ;
        CircleImageView profilePicture;

        public PostsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.context = itemView.getContext();

        }
    }
}
