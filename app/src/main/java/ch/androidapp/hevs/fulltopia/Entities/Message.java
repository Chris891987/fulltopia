package ch.androidapp.hevs.fulltopia.Entities;

/**
 * Created by Christian-Lenovo on 28.08.2017.
 */

public class Message {

    private String user;
    private String message;
    private String date;

    public Message() {
    }

    public String getUser() {
        return user;
    }

    public Message setUser(String user) {
        this.user = user;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Message setDate(String date) {
        this.date = date;
        return this;
    }
}
