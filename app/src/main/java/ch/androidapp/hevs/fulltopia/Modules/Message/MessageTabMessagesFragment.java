package ch.androidapp.hevs.fulltopia.Modules.Message;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewMessagesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.Entities.Message;
import ch.androidapp.hevs.fulltopia.Entities.User;
import ch.androidapp.hevs.fulltopia.R;


public class MessageTabMessagesFragment extends Fragment {

    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference users;
    private ArrayList<User> userList;
    private RecyclerView messageView;


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View v =  inflater.inflate( R.layout.message_fragment_tab_messages, container, false );

        //Reference to database
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();

        users = FirebaseDatabase.getInstance().getReference().child( "users" );
        userList = new ArrayList<User>();


        messageView = (RecyclerView) v.findViewById(R.id.recycler_view_messages);
        users.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User currentUser;
                for (DataSnapshot currentUserData : dataSnapshot.getChildren()){

                    currentUser = currentUserData.getValue(User.class);
                    currentUser.setKey( currentUserData.getKey() );
                    userList.add(currentUser);
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };

                RecyclerViewMessagesAdapter eventAdapter = new RecyclerViewMessagesAdapter(getActivity().getLayoutInflater(), userList);
                messageView.setLayoutManager(linearLayoutManager);
                messageView.setHasFixedSize(true);
                messageView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                messageView.setAdapter(eventAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });








        return v;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        return;
    }


}
