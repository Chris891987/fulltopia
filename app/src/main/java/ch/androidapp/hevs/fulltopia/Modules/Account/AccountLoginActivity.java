package ch.androidapp.hevs.fulltopia.Modules.Account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

/**
 * Login Page
 *
 */
public class AccountLoginActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private Button buttonLogin;

    private Toolbar toolBar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.account_login_activity );
        FulltopiaUtils.setContext( this );

        toolBar = (Toolbar) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( "Login Account" );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog( this );

        inputLayoutEmail = (TextInputLayout) findViewById( R.id.stepper_sub_category );
        inputLayoutPassword = (TextInputLayout) findViewById( R.id.user_login_input_password );
        buttonLogin = (Button) findViewById( R.id.user_btn_login );

        //listener on the Login button
        buttonLogin.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Boolean isFormValid = true;

                //check if email is valid
                if(!FulltopiaUtils.isValidEmail( inputLayoutEmail )){
                    isFormValid = false;
                }
                //check password
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutPassword )){
                    isFormValid = false;
                }
                if( isFormValid ){

                    String inputEmailValue = inputLayoutEmail.getEditText().getText().toString();
                    String inputPasswordValue = inputLayoutPassword.getEditText().getText().toString();
                    signInAccount( inputEmailValue, inputPasswordValue );
                }
            }
        } );
    }

    //compare the data entered with the database and errors management
    private void signInAccount( String email, String password ) {

        firebaseAuth.signInWithEmailAndPassword( email, password )
                .addOnCompleteListener( this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete( @NonNull Task<AuthResult> task ) {
                        if ( task.isSuccessful() ) {

                            progressDialog.setTitle( "Login Account" );
                            progressDialog.setMessage( "Please wait while we check your credentials" );
                            progressDialog.setCanceledOnTouchOutside( false );
                            progressDialog.show();

                            DatabaseReference currentUserReference = FirebaseDatabase.getInstance().getReference().child("users").child( FirebaseAuth.getInstance().getCurrentUser().getUid() );
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();

                            Map updateDeviceToken = new HashMap<String, String>();
                            updateDeviceToken.put("deviceToken", deviceToken);

                            currentUserReference.updateChildren( updateDeviceToken ).addOnCompleteListener( new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete( @NonNull Task<Void> task ) {

                                    Intent startIntent = new Intent( AccountLoginActivity.this, MenuActivity.class );
                                    startActivity( startIntent );
                                    progressDialog.dismiss();
                                    finish();
                                }
                            } );

                        } else {
                            try {
                            throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                inputLayoutPassword.setError(getString(R.string.error_weak_password));
                                inputLayoutPassword.requestFocus();
                            } catch(FirebaseAuthInvalidUserException e) {
                                inputLayoutEmail.setError(getString(R.string.error_invalid_email));
                                inputLayoutEmail.requestFocus();
                                inputLayoutPassword.setError(getString(R.string.error_invalid_email));
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                inputLayoutEmail.setError(getString(R.string.error_invalid_email));
                                inputLayoutEmail.requestFocus();
                                inputLayoutPassword.setError(getString(R.string.error_invalid_email));
                            } catch(FirebaseAuthUserCollisionException e) {
                                inputLayoutEmail.setError(getString(R.string.error_invalid_email));
                                inputLayoutEmail.requestFocus();
                            } catch(Exception e) {
                                Log.w("TIAGO", "signInWithEmail:failed", task.getException());
                            }
                            Toast.makeText( AccountLoginActivity.this, "Wrong email or password",
                                    Toast.LENGTH_SHORT ).show();
                            Log.w( "BUGGGGGGGGGG", "signInWithEmail:failed", task.getException() );
                        }
                    }
                } );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected( item );
    }
}
