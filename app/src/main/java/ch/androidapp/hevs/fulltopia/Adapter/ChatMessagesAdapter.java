package ch.androidapp.hevs.fulltopia.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;

import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.ChatMessage;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.EditProfileStepperActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.SetupStepperActivity;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.ItemRowHolder> {

    private ArrayList<ChatMessage> chatMessages;
    private Context context;
    private ItemRowHolder itemRowHolder;
    private String currentUserId;

    public ChatMessagesAdapter(Context context, ArrayList<ChatMessage> chatMessages ) {
        this.chatMessages = chatMessages;
        this.context = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder( ViewGroup viewGroup, int i ) {
        View v = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.single_chat_message, null );
        itemRowHolder = new ItemRowHolder( v );
        return itemRowHolder;
    }

    @Override
    public void onBindViewHolder( final ItemRowHolder itemRowHolder, int i ) {
        final ChatMessage chatMessage = chatMessages.get( i );

    }

    @Override
    public int getItemCount() {
        return (null != chatMessages ? chatMessages.size() : 0);
    }


    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout layoutContainer;
        protected TextView messageContainer;
        protected CircleImageView iconContainer;

        public ItemRowHolder( View view ) {
            super( view );
            this.layoutContainer = (RelativeLayout) view.findViewById( R.id.chat_single_layout );
            this.messageContainer = (TextView) view.findViewById( R.id.chat_single_message );
            this.iconContainer = (CircleImageView) view.findViewById( R.id.chat_single_icon );
        }
    }

}
