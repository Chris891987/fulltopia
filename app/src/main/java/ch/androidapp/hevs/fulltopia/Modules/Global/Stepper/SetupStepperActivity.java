package ch.androidapp.hevs.fulltopia.Modules.Global.Stepper;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.StartActivity;
import ch.androidapp.hevs.fulltopia.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

/**
 * Stepper activity
 */
public class SetupStepperActivity extends AppCompatActivity implements StepperLayout.StepperListener {

    private static final String CURRENT_STEP_POSITION_KEY = "position";

    protected StepperLayout mStepperLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // LayoutInflaterCompat.setFactory(getLayoutInflater(), new IconicsLayoutInflater(getDelegate()));
        super.onCreate(savedInstanceState);
        setTitle("Account Setup");

        setContentView( R.layout.abstract_layout_activity);
        //set the stepper layout
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        int startingStepPosition = savedInstanceState != null ? savedInstanceState.getInt(CURRENT_STEP_POSITION_KEY) : 0;
        mStepperLayout.setAdapter(new SampleFragmentStepAdapter(getSupportFragmentManager(), this), startingStepPosition);

        mStepperLayout.setListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_STEP_POSITION_KEY, mStepperLayout.getCurrentStepPosition());
        super.onSaveInstanceState(outState);
    }

    //behavior of back button clicked
    @Override
    public void onBackPressed() {
        final int currentStepPosition = mStepperLayout.getCurrentStepPosition();
        if (currentStepPosition > 0) {
            mStepperLayout.onBackClicked();
        } else {
            FirebaseAuth.getInstance().signOut();
            Intent startIntent = new Intent( this, StartActivity.class );
            startActivity( startIntent );
        }
    }

    //registration in firebase
    @Override
    public void onCompleted(View completeButton) {
        //Toast.makeText(this, "onCompleted!", Toast.LENGTH_SHORT).show();

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase.getInstance().getReference().child( "users" ).child( firebaseAuth.getCurrentUser().getUid() )
                .child( "hasCompleteRegistration" ).setValue( "true" );
        Intent startIntent = new Intent( this, MenuActivity.class );
        startActivity( startIntent );
        finish();
    }

    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        //Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReturn() {
        FirebaseAuth.getInstance().signOut();
        Intent startIntent = new Intent( this, StartActivity.class );
        startActivity( startIntent );
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Toast.makeText(this,"START", Toast.LENGTH_SHORT );
        super.onActivityResult(requestCode,resultCode,data);

    }




}
