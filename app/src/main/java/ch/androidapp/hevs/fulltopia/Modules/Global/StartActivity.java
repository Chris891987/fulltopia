package ch.androidapp.hevs.fulltopia.Modules.Global;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ch.androidapp.hevs.fulltopia.Modules.Account.AccountLoginActivity;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountRegisterActivity;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Start application
 */
public class StartActivity extends AppCompatActivity {

    private Button buttonRegister;
    private Button buttonLogin;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.start_activity );

        buttonRegister = (Button) findViewById( R.id.stepper_profile_change_picture);
        buttonLogin = (Button) findViewById( R.id.start_btn_login );

        //behavior when click on register -> start register activity
        buttonRegister.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Intent startIntent = new Intent( StartActivity.this, AccountRegisterActivity.class );
                startActivity( startIntent );
            }
        } );

        //behavior when click on register -> start login activity
        buttonLogin.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Intent startIntent = new Intent( StartActivity.this, AccountLoginActivity.class );
                startActivity( startIntent );
            }
        } );
    }
}
