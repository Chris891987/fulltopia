package ch.androidapp.hevs.fulltopia.Services;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ch.androidapp.hevs.fulltopia.Interface.OnGetOnceFirebasedataListener;

/**
 * Created by Rexy on 09.09.2017.
 */

public class FirebaseMistakes {


    private ValueEventListener mListener;
    private int counter = 0;
    public  ValueEventListener readDataOnce(String child, final OnGetOnceFirebasedataListener listener) {
        listener.onStart();
        FirebaseDatabase.getInstance().getReference().child(child).addListenerForSingleValueEvent( mListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
            }
        });
        return mListener;
    }

}
