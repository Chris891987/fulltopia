package ch.androidapp.hevs.fulltopia.Modules.DashBoard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Christian-Lenovo on 14.07.2017.
 */

public class PagerAdapterDashboard extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterDashboard( FragmentManager fm, int NumOfTabs ) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem( int position ) {

        switch (position) {
            case 0:
                DashboardTabEventsFragment tab1 = new DashboardTabEventsFragment();
                return tab1;
            case 1:
                DashboardTabCommunitiesFragment tab2 = new DashboardTabCommunitiesFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}


