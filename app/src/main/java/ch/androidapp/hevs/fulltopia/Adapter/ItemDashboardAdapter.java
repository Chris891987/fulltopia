package ch.androidapp.hevs.fulltopia.Adapter;

/**
 * Created by Christian-Lenovo on 22.08.2017.
 * ADAPTER FOR DASHBOARD ITEMS
 */

import ch.androidapp.hevs.fulltopia.Entities.AbstractObject;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.Entities.SectionDataModel;
import ch.androidapp.hevs.fulltopia.R;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class ItemDashboardAdapter extends RecyclerView.Adapter<ItemDashboardAdapter.ItemRowHolder>{

    private ArrayList<SectionDataModel> dataList;
    private Context mContext;

    public ItemDashboardAdapter(Context context, ArrayList<SectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if(!(this.dataList.isEmpty())) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
            ItemRowHolder mh = new ItemRowHolder(v);
            return mh;
        }
        return null;
    }

    @Override
    public void onBindViewHolder( ItemRowHolder itemRowHolder, int i ) {
        final String sectionName = dataList.get(i).getHeaderTitle();
        ArrayList<AbstractObject> singleSectionAbstractObjects = dataList.get(i).getAllItemsInSection();
        itemRowHolder.titleSection.setText( sectionName );
        SectionDashboardAdapter itemListDataAdapter = new SectionDashboardAdapter( mContext, singleSectionAbstractObjects);

        if(singleSectionAbstractObjects.get(0) instanceof Community )
        {
            ViewGroup.LayoutParams params = itemRowHolder.recycler_view_list.getLayoutParams();
            params.height = 430;
            itemRowHolder.recycler_view_list.setLayoutParams(params);
        }

        itemRowHolder.recycler_view_list.setHasFixedSize( true );
        itemRowHolder.recycler_view_list.setLayoutManager( new LinearLayoutManager( mContext, LinearLayoutManager.HORIZONTAL, false ) );
        itemRowHolder.recycler_view_list.setAdapter( itemListDataAdapter );
        itemRowHolder.recycler_view_list.setNestedScrollingEnabled( false );

        itemRowHolder.btnMore.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                Toast.makeText( v.getContext(), "click event on more, "+sectionName , Toast.LENGTH_SHORT ).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ( null != dataList ? dataList.size() : 0 );
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView titleSection;
        protected RecyclerView recycler_view_list;
        protected Button btnMore;

        public ItemRowHolder( View view ) {
            super( view );
            this.titleSection = ( TextView ) view.findViewById( R.id.titleSection );
            this.recycler_view_list = ( RecyclerView ) view.findViewById( R.id.recycler_view_list );
            this.btnMore= ( Button ) view.findViewById( R.id.btnMore );
        }
    }
}
