package ch.androidapp.hevs.fulltopia.Modules.Agenda;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Christian-Lenovo on 14.07.2017.
 */

public class PagerAdapterAgenda extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterAgenda( FragmentManager fm, int NumOfTabs ) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem( int position ) {

        switch (position) {
            case 0:
                AgendaTabPastFragment tab1 = new AgendaTabPastFragment();
                return tab1;
            case 1:
                AgendaTabFuturFragment tab2 = new AgendaTabFuturFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}


