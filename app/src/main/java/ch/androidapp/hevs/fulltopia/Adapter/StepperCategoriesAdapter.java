package ch.androidapp.hevs.fulltopia.Adapter;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.EditProfileStepperActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.SetupStepperActivity;
import ch.androidapp.hevs.fulltopia.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;

/**
 * This class allows the display of the stepper
 */
public class StepperCategoriesAdapter extends RecyclerView.Adapter<StepperCategoriesAdapter.ItemRowHolder> {

    private ArrayList<Category> categories;
    private Boolean isChecked;
    private Context context;
    private String currentUserId;
    private ItemRowHolder mh;

    public StepperCategoriesAdapter( Context context, ArrayList<Category> categories ) {
        this.categories = categories;
        this.context = context;


        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        this.currentUserId = firebaseAuth.getCurrentUser().getUid();

    }

    @Override
    public ItemRowHolder onCreateViewHolder( ViewGroup viewGroup, int i ) {
        View v = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.stepper_category_item, null );
        mh = new ItemRowHolder( v );
        return mh;
    }

    @Override
    public void onBindViewHolder( final ItemRowHolder itemRowHolder, int i ) {
        final Category category = categories.get( i );
        itemRowHolder.title.setText( category.getName() );
        itemRowHolder.title.setTextColor( Color.parseColor( category.getTextColor() ) );
        itemRowHolder.card.setCardBackgroundColor( Color.parseColor( category.getBackgroundColor() ) );
        itemRowHolder.iconChecked.setColorFilter( Color.parseColor( category.getTextColor() ) );
        Log.w( "JACK", " category.getChecked " + category.toString() );
        if ( !category.getChecked() ) {
            itemRowHolder.iconChecked.setVisibility( View.INVISIBLE );
        } else {
            itemRowHolder.iconChecked.setVisibility( View.VISIBLE );
        }

        itemRowHolder.iconCategory.setImageDrawable(
                new IconicsDrawable( this.context,
                        category.getIconName() ).sizeDp( 64 ).color( Color.parseColor( category.getTextColor() ) )
        );

        itemRowHolder.card.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                isChecked = itemRowHolder.iconChecked.getVisibility() != View.VISIBLE;

                if ( !isChecked ) {
                    itemRowHolder.iconChecked.setVisibility( View.INVISIBLE );
                } else {
                    itemRowHolder.iconChecked.setVisibility( View.VISIBLE );
                }
                category.setChecked( isChecked );

                //Gestion du click dans le stepper
                if ( context.getClass() == SetupStepperActivity.class ||  context.getClass() == EditProfileStepperActivity.class ) {
                    getSetupStepperCategoryClickBehavior( category );
                }
                else {
                    getFavoritedSearchCategoriesClickBehavior( category );
                }
            }
        } );
    }

    @Override
    public int getItemCount() {
        return (null != categories ? categories.size() : 0);
    }

    public Category getItem(int position) {
        return categories.get( position );
    }

    public void setCheck(int position, boolean isChecked) {
         categories.get( position ).setChecked(isChecked);

    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected CardView card;
        protected TextView title;
        protected ImageView iconCategory;
        protected ImageView iconChecked;


        public ItemRowHolder( View view ) {
            super( view );
            this.title = (TextView) view.findViewById( R.id.item_title );
            this.card = (CardView) view.findViewById( R.id.card_category );
            this.iconCategory = (ImageView) view.findViewById( R.id.iconCategory );
            this.iconChecked = (ImageView) view.findViewById( R.id.iconChecked );
        }
    }

    private void getSetupStepperCategoryClickBehavior( Category category ) {

        Log.w( "TIAGO", " CLICK ON " + category.toString() );
        DatabaseReference referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child( "users" ).child( FirebaseAuth.getInstance().getCurrentUser().getUid() ).child("appreciatedCategories");
        if(category.getChecked()){
            referenceCurrentUser.child( category.getKey() ).setValue(true);
        }else{
            referenceCurrentUser.child( category.getKey() ).getRef().removeValue();
        }

    }

    private void getFavoritedSearchCategoriesClickBehavior( Category category ) {

        Log.w( "JACK", " CLICK ON " + category.toString() );
        DatabaseReference referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child( "users" ).child( FirebaseAuth.getInstance().getCurrentUser().getUid() ).child("favoritedSearch");
        if(category.getChecked()){
            referenceCurrentUser.child( category.getKey() ).setValue(true);
        }else{
            referenceCurrentUser.child( category.getKey() ).getRef().removeValue();
        }

    }
}
