package ch.androidapp.hevs.fulltopia.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import ch.androidapp.hevs.fulltopia.Entities.Notification;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Rexy on 08.09.2017.
 */

public class FirebaseServiceMessaging extends FirebaseMessagingService{
    private  NotificationCompat.Builder mBuilder;

    @Override
    public void onMessageReceived( RemoteMessage remoteMessage ) {
        super.onMessageReceived( remoteMessage );

        Notification notification = new Notification();
        notification.setTitle(  remoteMessage.getNotification().getTitle() );
        notification.setBody(  remoteMessage.getNotification().getBody() );
        notification.setIcon(  remoteMessage.getNotification().getIcon() );
        notification.setClickAction(  remoteMessage.getNotification().getClickAction() );
        notification.setNotificationType(  remoteMessage.getData().get("notificationType") );
        notification.setUserReceiverKey(  remoteMessage.getData().get("userReceiverKey") );


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon( R.mipmap.ic_launcher )
                        .setContentTitle(notification.getTitle())
                        .setContentText(notification.getBody());

        int mNotificationId = (int)System.currentTimeMillis();
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

        Intent resultIntent = new Intent(notification.getClickAction());

        Log.w("TIAGO", notification.getUserReceiverKey());
        switch ( notification.getNotificationType() ){
            case "chat":
                resultIntent.putExtra( "friendUserId", notification.getUserReceiverKey() );
                break;
        }

        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent( resultPendingIntent );
    }
}
