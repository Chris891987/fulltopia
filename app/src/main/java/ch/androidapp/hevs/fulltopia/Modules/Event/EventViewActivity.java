package ch.androidapp.hevs.fulltopia.Modules.Event;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewPostAdapter;
import ch.androidapp.hevs.fulltopia.Adapter.SingleItemAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Entities.Post;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Christian-Lenovo on 04.09.2017.
 */

public class EventViewActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private static final String TAG = EventViewActivity.class.getSimpleName();

    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private DatabaseReference referenceCatEvent;
    private DatabaseReference userEvents;
    private ImageView imageView;
    private EditText nameEvent;
    private EditText descEvent;
    private EditText locationEvent;
    private TextView startDate;
    private TextView endDate;
    private TextView minPartecipants;
    private TextView maxPartecipants;
    private TextView actualPartecipants;
    private LinearLayout viewCategories;
    private Button joinEvent;
    private Button leaveEvent;
    private Event event;
    private Toolbar toolBar;
    private LatLng myPosition;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private ValueEventListener mListener;
    private boolean isOwner = false;
    private String idEvent;
    private HashMap<String,Boolean> users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set content
        setContentView(R.layout.event_view_activity);

        //Add toolbar
        toolBar = (Toolbar) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( R.string.event_view_title );
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        //Get Id from intent
        idEvent = getIntent().getStringExtra("id");

        //Recovering reference from XML file
        joinEvent = (Button) findViewById( R.id.button_Join );
        leaveEvent = (Button) findViewById( R.id.button_Leave );
        imageView = (ImageView) findViewById( R.id.imageViewEvent );
        nameEvent = (EditText) findViewById( R.id.nameEvent );
        descEvent = (EditText) findViewById( R.id.descEvent );
        locationEvent = (EditText) findViewById( R.id.locationEvent );
        startDate = (TextView) findViewById( R.id.textView_title_start_date );
        endDate = (TextView) findViewById( R.id.textView_title_end_date );
        minPartecipants = (TextView) findViewById( R.id.textView_min_participants );
        maxPartecipants = (TextView) findViewById( R.id.textView_max_participants );
        actualPartecipants = (TextView) findViewById( R.id.textView_actual_participants );
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.mapView );
        viewCategories = (LinearLayout) findViewById( R.id.view_categorie );

        //Setting map
        mapFragment.getMapAsync( this );

        //Putting editText not enabled
        nameEvent.setEnabled(false);
        descEvent.setEnabled(false);
        locationEvent.setEnabled(false);

        ///////////// Dummy post //////////////////////
        ArrayList<Post> postsList = new ArrayList<Post>();
        Post post = new Post();
        post.setUser("Jack Herren").setMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.").setDate("25.05.1320");
        postsList.add(post);

        Post post1 = new Post();
        post1.setUser("Jack Herren2").setMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.").setDate("25.05.1320");
        postsList.add(post1);


        ///////////////////////////////////////////////

        //Recover user
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();

        //Setting the recycler view for messages
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        RecyclerView msgView = (RecyclerView) findViewById(R.id.recycler_view_list);
        msgView.setLayoutManager(linearLayoutManager);
        RecyclerViewPostAdapter postAdapter = new RecyclerViewPostAdapter(this.getLayoutInflater(), postsList);
        msgView.setAdapter(postAdapter);


        //Recovering the event
        mDatabase = FirebaseDatabase.getInstance().getReference("events");
        mDatabase.child(idEvent).addValueEventListener(mListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Retrievieng data
                event = dataSnapshot.getValue(Event.class);
                nameEvent.setText(event.getName());
                descEvent.setText(event.getDescription());
                locationEvent.setText(event.getLocationName());
                startDate.setText(getString(R.string.startdate)+" "+event.getStartDate());
                endDate.setText(getString(R.string.enddate)+" "+event.getEndDate());

                //Setting info partecipants
                if(event.getMinPartecipants() > 0) {
                    minPartecipants.setText(getString(R.string.minPartecipants)+" " + event.getMinPartecipants().toString());
                    maxPartecipants.setText(getString(R.string.maxMembers)+" " + event.getMaxPartecipants().toString());
                }
                else{
                    minPartecipants.setText(getString(R.string.minPartecipants)+" Unlimited");
                    maxPartecipants.setVisibility(View.GONE);
                }
                //Count user in this event
                if(event.getUsersJoined() == null){
                    actualPartecipants.setText(getString(R.string.actualPartecipants)+" 0");
                }
                else {
                    actualPartecipants.setText(getString(R.string.actualPartecipants) + " " + event.getUsersJoined().size());
                }
                //Hide/show block categories
                if(event.getCategories() == null){
                    //If any categorie hide this block
                    viewCategories.setVisibility(View.GONE);
                }
                else {
                    //Setting the recycler view for categories
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EventViewActivity.this) {
                        @Override
                        public boolean canScrollVertically() {
                            return false;
                        }
                    };
                    RecyclerView catView = (RecyclerView) findViewById(R.id.recycler_view_categories);
                    catView.setLayoutManager(linearLayoutManager);
                    SingleItemAdapter catAdapter = new SingleItemAdapter(getLayoutInflater(), event.getCategories());
                    catView.setAdapter(catAdapter);
                }

                //The owner of the event can modify/delete the event
                if(currentUser.getUid().toString().equals(event.getOwner().toString())){
                    isOwner = true;
                    joinEvent.setVisibility( View.GONE );
                    invalidateOptionsMenu(); // now onCreateOptionsMenu(...) is called again
                }else if(event.getUsersJoined() != null)
                    //Prepare the correct button join/leave
                    if(event.getUsersJoined().containsKey( currentUser.getUid().toString() )){
                        joinEvent.setVisibility( View.GONE );
                        leaveEvent.setVisibility( View.VISIBLE );
                    }

                //Updating the map
                myPosition = new LatLng(  event.getLocationLatitude(),  event.getLocationLongitude() );
                mMap.addMarker( new MarkerOptions().position( myPosition ));
                mMap.moveCamera( CameraUpdateFactory.newLatLng( myPosition ));
                mMap.setMinZoomPreference( 14.0f );

                //Has set image ?
                if(!event.getUrlDefault()) {
                    //Recovering the image stored on firebase
                    mStorage = FirebaseStorage.getInstance().getReference("pictures/events/" + idEvent);
                    mStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            // Load the image using Glide
                            Glide.with(getApplication())
                                    .using(new FirebaseImageLoader())
                                    .load(mStorage)
                                    .signature(new StringSignature(event.getUpdateAt().toString()))
                                    .into(imageView);
                        }
                    });
                }
                else{
                    //If image not set, put the default image
                    Glide.with(getApplication())
                            .load(R.drawable.no_image)
                            .into(imageView);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        //Leave the event
        leaveEvent.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                joinEvent.setVisibility(View.VISIBLE);
                leaveEvent.setVisibility(View.GONE);
                users = event.getUsersJoined();
                users.remove(currentUser.getUid());
                mDatabase.child(idEvent).child("usersJoined").setValue(users);

                //Update our agenda
                userEvents = FirebaseDatabase.getInstance().getReference("users_events");
                userEvents.child(currentUser.getUid().toString()).child(idEvent).removeValue();
            }
        } );

        //Join the event
        joinEvent.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                joinEvent.setVisibility(View.GONE);
                leaveEvent.setVisibility(View.VISIBLE);
                if(event.getUsersJoined() == null)
                    users = new HashMap<>();
                else
                    users = event.getUsersJoined();
                //Update on the cloud
                users.put(currentUser.getUid(),true);
                mDatabase.child(idEvent).child("usersJoined").setValue(users);

                //Update our agenda
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                userEvents = FirebaseDatabase.getInstance().getReference("users_events");
                try {
                    userEvents.child(currentUser.getUid().toString()).child(idEvent).child("time").setValue(dateFormat.parse(event.getStartDate()).getTime());
                   System.out.println(dateFormat.parse(event.getStartDate()).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        } );

    }

    //Prepare google map
    @Override
    public void onMapReady( GoogleMap googleMap ) {
        mMap = googleMap;
    }

    //Connection to google service failed
    @Override
    public void onConnectionFailed( @NonNull ConnectionResult result ) {
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        if(isOwner){
            getMenuInflater().inflate(R.menu.viewevent, menu);
            return super.onCreateOptionsMenu(menu);
        }
        else{
            return super.onCreateOptionsMenu(menu);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                //Close the activity
                Intent intent = new Intent(EventViewActivity.this, MenuActivity.class);
                startActivity(intent);
                return true;
            case R.id.eventEdit:
                //Putting editText not enabled and sending the data with Json
                event.setId(idEvent);
                Intent activity = new Intent(EventViewActivity.this , EventCreateActivity.class);
                activity.putExtra("event", new Gson().toJson(event));
                startActivity(activity);
                return true;
            case R.id.eventDelete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle(getString(R.string.title_event_delete));
                alertDialogBuilder
                        .setMessage(getString(R.string.message_event_delete))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Remove the event
                                mDatabase.child(idEvent).removeEventListener(mListener);
                                //Remove the image
                                if(!event.getUrlDefault())
                                    mStorage.delete();
                                mDatabase.child(idEvent).removeValue();
                                //Remove categorie relatesa
                                referenceCatEvent = FirebaseDatabase.getInstance().getReference().child( "categories_events" );
                                if(event.getCategories() != null){
                                    Iterator it = event.getCategories().entrySet().iterator();
                                    while (it.hasNext()) {
                                        Map.Entry pair = (Map.Entry)it.next();
                                        referenceCatEvent.child(String.valueOf(pair.getKey())).child(idEvent).removeValue();
                                        it.remove();
                                    }
                                }
                                //Go to menu
                                Intent startIntent = new Intent( EventViewActivity.this, MenuActivity.class );
                                startActivity( startIntent );
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }
    
}
