package ch.androidapp.hevs.fulltopia.Modules.Global;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Christian-Lenovo on 05.09.2017.
 * Picker for set time
 */

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    public static final int FLAG_START_TIME = 0;
    public static final int FLAG_END_TIME = 1;
    private int flag = 0;
    private TextView textViewStartDate;
    private TextView textViewEndDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        //Create and return a new instance of TimePickerDialog
        return new TimePickerDialog(getActivity(),this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void setFlag(int i) {
        flag = i;
    }

    //onTimeSet() callback method
    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        if (flag == FLAG_START_TIME) {
            textViewStartDate = (TextView) getActivity().findViewById( R.id.textview_set_start_date );
            textViewStartDate.append(" "+String.format("%02d",hourOfDay)+":"+String.format("%02d",minute));

        } else if (flag == FLAG_END_TIME) {
            textViewEndDate = (TextView) getActivity().findViewById( R.id.textview_set_end_date );
            textViewEndDate.append(" "+String.format("%02d",hourOfDay)+":"+String.format("%02d",minute));

        }

    }
}
