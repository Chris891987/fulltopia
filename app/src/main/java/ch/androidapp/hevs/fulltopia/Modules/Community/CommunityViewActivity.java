package ch.androidapp.hevs.fulltopia.Modules.Community;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewPostAdapter;
import ch.androidapp.hevs.fulltopia.Adapter.SingleItemAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.Entities.Post;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;

import ch.androidapp.hevs.fulltopia.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CommunityViewActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private static final String TAG = CommunityViewActivity.class.getSimpleName();

    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private ImageView imageView;
    private EditText nameCommunity;
    private EditText descCommunity;
    private EditText locationCommunity;
    private Button joinCommunity;
    private Button leaveCommunity;
    private LinearLayout viewCategories;
    private Community community;
    private Toolbar toolBar;
    private LatLng myPosition;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference referenceCatComm;
    private ValueEventListener mListener;
    private TextView actualPartecipants;
    private boolean isOwner = false;
    private String idCommunity;
    private HashMap<String,Boolean> users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_view_activity);

        //Recovering intent
        idCommunity = getIntent().getStringExtra("id");

        //Add toolbar
        toolBar = (Toolbar) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( R.string.view_community_title );
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        //Recovering reference from XML file
        joinCommunity = (Button) findViewById( R.id.button_Join );
        leaveCommunity = (Button) findViewById( R.id.button_Leave );
        imageView = (ImageView) findViewById( R.id.imageViewCommunity );
        nameCommunity = (EditText) findViewById( R.id.nameCommunity );
        descCommunity = (EditText) findViewById( R.id.descCommunity );
        locationCommunity = (EditText) findViewById( R.id.locationCommunity );
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.mapView );
        viewCategories = (LinearLayout) findViewById( R.id.view_categorie );
        actualPartecipants = (TextView) findViewById( R.id.textView_actual_participants );

        //Setting map
        mapFragment.getMapAsync( this );

        //Putting editText not enabled
        nameCommunity.setEnabled(false);
        descCommunity.setEnabled(false);
        locationCommunity.setEnabled(false);

        ///////////// Dummy post //////////////////////
        ArrayList<Post> postsList = new ArrayList<Post>();
        Post post = new Post();
        post.setUser("Jack Herren").setMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.").setDate("25.05.1320");
        postsList.add(post);

        Post post1 = new Post();
        post1.setUser("Jack Herren2").setMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.").setDate("25.05.1320");
        postsList.add(post1);


        ///////////////////////////////////////////////

        //Recover user
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();

        //Setting the recycler view for message
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        RecyclerView msgView = (RecyclerView) findViewById(R.id.recycler_view_list);
        msgView.setLayoutManager(linearLayoutManager);
        RecyclerViewPostAdapter postAdapter = new RecyclerViewPostAdapter(this.getLayoutInflater(), postsList);
        msgView.setAdapter(postAdapter);


        //Recovering the community
        mDatabase = FirebaseDatabase.getInstance().getReference("communities");
        mDatabase.child(idCommunity).addValueEventListener(mListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                community = dataSnapshot.getValue(Community.class);
                nameCommunity.setText(community.getName());
                descCommunity.setText(community.getDescription());
                locationCommunity.setText(community.getLocationName());

                //Hide block categories if doesn't exist
                if(community.getCategories() == null){
                    viewCategories.setVisibility(View.GONE);
                }
                else {
                    //Setting the recycler view for categories
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CommunityViewActivity.this) {
                        @Override
                        public boolean canScrollVertically() {
                            return false;
                        }
                    };
                    RecyclerView catView = (RecyclerView) findViewById(R.id.recycler_view_categories);
                    catView.setLayoutManager(linearLayoutManager);
                    SingleItemAdapter catAdapter = new SingleItemAdapter(getLayoutInflater(), community.getCategories());
                    catView.setAdapter(catAdapter);
                }

                //The owner of the event can modify/delete the community
                if(currentUser.getUid().toString().equals(community.getOwner().toString())){
                    //Menu visibilty (only owner can modify/delete)
                    isOwner = true;
                    joinCommunity.setVisibility(View.GONE);
                    invalidateOptionsMenu(); // now onCreateOptionsMenu(...) is called again
                }else if(community.getUsersJoined() != null)
                    //Prepare the correct button join/leave
                    if(community.getUsersJoined().containsKey(currentUser.getUid().toString())){
                        joinCommunity.setVisibility(View.GONE);
                        leaveCommunity.setVisibility(View.VISIBLE);
                }

                //Count users
                if(community.getUsersJoined() == null){
                    actualPartecipants.setText(getString(R.string.actualPartecipants)+" 0");
                }
                else {
                    actualPartecipants.setText(getString(R.string.actualPartecipants) + " " + community.getUsersJoined().size());
                }

                //Updating the map
                myPosition = new LatLng(  community.getLocationLatitude() ,  community.getLocationLongitude() );
                mMap.addMarker(new MarkerOptions().position( myPosition ));
                mMap.moveCamera(CameraUpdateFactory.newLatLng( myPosition ));
                mMap.setMinZoomPreference(14.0f);

                //Has set image ?
                if(!community.getUrlDefault()){
                    //Recovering the image stored on firebase
                    mStorage = FirebaseStorage.getInstance().getReference("pictures/communities/" + idCommunity);
                    mStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            // Load the image using Glide
                            Glide.with(getApplication())
                                    .using(new FirebaseImageLoader())
                                    .load(mStorage)
                                    .into(imageView);
                        }
                    });
                }
                else{
                    Glide.with(getApplication())
                            .load(R.drawable.no_image)
                            .into(imageView);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        //Leave the community
        leaveCommunity.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                joinCommunity.setVisibility(View.VISIBLE);
                leaveCommunity.setVisibility(View.GONE);
                HashMap<String,Boolean> users =community.getUsersJoined();
                users.remove(currentUser.getUid());
                mDatabase.child(idCommunity).child("usersJoined").setValue(users);
            }
        } );

        //Join the community
        joinCommunity.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                joinCommunity.setVisibility(View.GONE);
                leaveCommunity.setVisibility(View.VISIBLE);
                if(community.getUsersJoined() == null)
                    users = new HashMap<>();
                else
                    users = community.getUsersJoined();
                users.put(currentUser.getUid(),true);
                mDatabase.child(idCommunity).child("usersJoined").setValue(users);
            }
        } );

    }

    //Prepare google map
    @Override
    public void onMapReady( GoogleMap googleMap ) {
        mMap = googleMap;
    }

    //Connection to google service failed
    @Override
    public void onConnectionFailed( @NonNull ConnectionResult result ) {
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        if(isOwner){
            getMenuInflater().inflate(R.menu.viewcommunity, menu);
            return super.onCreateOptionsMenu(menu);
        }
        else{
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case android.R.id.home:
                //Close the activity
                Intent intent = new Intent(CommunityViewActivity.this, MenuActivity.class);
                startActivity(intent);
                return true;
            case R.id.communityEdit:
                //Putting editText not enabled
                community.setId(idCommunity);
                Intent activity = new Intent(CommunityViewActivity.this , CommunityCreateActivity.class);
                activity.putExtra("community", new Gson().toJson(community));
                startActivity(activity);
                return true;
            case R.id.communityDelete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle(getString(R.string.title_community_delete));
                alertDialogBuilder
                        .setMessage(getString(R.string.message_community_delete))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Remove the community
                                mDatabase.child(idCommunity).removeEventListener(mListener);

                                //Remove the image
                                if(!community.getUrlDefault())
                                    mStorage.delete();
                                mDatabase.child(idCommunity).removeValue();
                                //Remove categorie relatesa
                                referenceCatComm = FirebaseDatabase.getInstance().getReference().child( "categories_communities" );
                                if(community.getCategories() != null){
                                    Iterator it = community.getCategories().entrySet().iterator();
                                    while (it.hasNext()) {
                                        Map.Entry pair = (Map.Entry)it.next();
                                        referenceCatComm.child(String.valueOf(pair.getKey())).child(idCommunity).removeValue();
                                        it.remove();
                                    }
                                }
                                //Go to menu
                                Intent startIntent = new Intent( CommunityViewActivity.this, MenuActivity.class );
                                startActivity( startIntent );
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }



}
