package ch.androidapp.hevs.fulltopia.Modules.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Christian-Lenovo on 14.07.2017.
 */

public class PagerAdapterMessages extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterMessages( FragmentManager fm, int NumOfTabs ) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem( int position ) {

        switch (position) {
            case 0:
                MessageTabMessagesFragment tab1 = new MessageTabMessagesFragment();
                return tab1;
            case 1:
                MessageTabNotificationsFragment tab2 = new MessageTabNotificationsFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}


