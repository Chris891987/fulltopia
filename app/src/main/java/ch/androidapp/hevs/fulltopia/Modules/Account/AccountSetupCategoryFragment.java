package ch.androidapp.hevs.fulltopia.Modules.Account;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ch.androidapp.hevs.fulltopia.Adapter.StepperCategoriesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class AccountSetupCategoryFragment extends Fragment implements Step {
    private TextInputLayout inputLayoutEmail;
    private Button buttonLogin;

    private View rootView;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference referenceCurrentUser;

    private ArrayList<Category> allCategories = new ArrayList<Category>();

    private DatabaseReference referenceCategories;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        this.rootView = inflater.inflate( R.layout.account_setup_category_fragment, container, false );
        FulltopiaUtils.setContext( getContext() );

        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        String currentUserId = currentUser.getUid();

        allCategories = new ArrayList<Category>();


        //Remplir categories
        referenceCategories = FirebaseDatabase.getInstance().getReference().child( "categories" );
        referenceCategories.orderByChild( "childs" ).startAt( "" ).addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                for ( DataSnapshot currentCategory : dataSnapshot.getChildren() ) {

                    Category newCategory = new Category( currentCategory.getKey() );
                    for ( DataSnapshot currentCategoryItem : currentCategory.getChildren() ) {

                        switch ( currentCategoryItem.getKey() ) {
                            case "name":
                                newCategory.setName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "iconName":
                                newCategory.setIconName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "textColor":
                                newCategory.setTextColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "backgroundColor":
                                newCategory.setBackgroundColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            default:
                                break;
                        }
                    }
                    allCategories.add( newCategory );
                }


                DatabaseReference referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child( "users" ).child( currentUser.getUid() );
                referenceCurrentUser.child("appreciatedCategories").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userCategories) {

                        if(userCategories.exists()){

                            for ( Category currentCategoryInList : allCategories ) {
                                for (DataSnapshot currentCategory : userCategories.getChildren()) {
                                    if(currentCategoryInList.getKey().toString().equals(currentCategory.getKey())){
                                        currentCategoryInList.setChecked( true );

                                        Log.w("TITI", currentCategoryInList.getKey());
                                    }
                                }
                            }
                        }



                        Log.w("TITI", allCategories.toString());

                        StepperCategoriesAdapter adapter = new StepperCategoriesAdapter( getContext(), allCategories );

                        RecyclerView categoriesRecyclerView = (RecyclerView) rootView.findViewById( R.id.categoriesRecyclerView );
                        categoriesRecyclerView.setHasFixedSize( true );
                        categoriesRecyclerView.setAdapter( adapter );

                        // Manage if portrait or landscape
                        int col;
                        if ( getActivity().getResources().getConfiguration().orientation == 1 )
                            col = 2;// Portrait => 2 colums
                        else
                            col = 3;// Landscape => 3 colums

                        categoriesRecyclerView.setLayoutManager( new GridLayoutManager( getContext(), col ) );
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        } );


        return rootView;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError( @NonNull VerificationError error ) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

}