package ch.androidapp.hevs.fulltopia.Entities;

import java.util.ArrayList;

/**
 * Created by Christian-Lenovo on 22.08.2017.
 */

public class SectionDataModel {

    private String headerTitle;
    private ArrayList<AbstractObject> allItemsInSection;


    public SectionDataModel() {

    }
    public SectionDataModel( String headerTitle, ArrayList<AbstractObject> allItemsInSection ) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<AbstractObject> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection( ArrayList<AbstractObject> allItemsInSection ) {
        this.allItemsInSection = allItemsInSection;
    }

}
