package ch.androidapp.hevs.fulltopia.Entities;

/**
 * Created by Christian-Lenovo on 28.08.2017.
 */

public class Post {

    private String fromUser;
    private String message;
    private String date;

    public Post() {
    }

    public String getUser() {
        return fromUser;
    }

    public Post setUser(String user) {
        this.fromUser = user;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Post setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Post setDate(String date) {
        this.date = date;
        return this;
    }
}
