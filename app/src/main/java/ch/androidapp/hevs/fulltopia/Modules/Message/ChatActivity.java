package ch.androidapp.hevs.fulltopia.Modules.Message;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewMessagesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.ChatMessage;
import ch.androidapp.hevs.fulltopia.Entities.Notification;
import ch.androidapp.hevs.fulltopia.Entities.User;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

import static ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils.getContext;

public class ChatActivity extends AppCompatActivity {

    private String friendUserId;
    private String friendUserName;
    private String friendUserPhoto;
    private Toolbar chatToolBar;

    private ArrayList<ChatMessage> messageList;

    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference users;
    private DatabaseReference rootReference;
    private String currentUserId;

    private TextView profileDisplayName;
    private TextView profileStatus;
    private CircleImageView profileImage;

    private EditText editTextInput;
    private ImageButton sendMessageButton;

    private RecyclerView messageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_chat_activity);

        chatToolBar = (Toolbar) findViewById(R.id.app_bar_layout);
        setSupportActionBar(chatToolBar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled( true );
        actionBar.setDisplayShowCustomEnabled( true );
        actionBar.setTitle( friendUserId );

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = inflater.inflate( R.layout.chat_custom_bar, null );

        actionBar.setCustomView(actionBarView);

        friendUserId = getIntent().getStringExtra( "friendUserId" );

        rootReference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        currentUserId = currentUser.getUid();

        DatabaseReference currentUserReference =  rootReference.child("users").child( currentUser.getUid() );
        DatabaseReference friendUserReference =  rootReference.child("users").child( friendUserId );


        profileDisplayName = (TextView) findViewById(R.id.chat_bar_display_name);
        profileStatus = (TextView) findViewById(R.id.chat_bar_status);
        profileImage = (CircleImageView) findViewById(R.id.chat_bar_img);

        friendUserReference.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User friendUser = dataSnapshot.getValue(User.class);
                profileDisplayName.setText(friendUser.getName());
                friendUserName = friendUser.getName();
                profileStatus.setText("Status not implemented yet");
                if(!friendUser.getProfile_picture_thumbnail().equals("default")){
                    Picasso.with( getApplicationContext() ).load( friendUserPhoto ).placeholder(R.drawable.no_profile_picture).into( profileImage );
                }

            }



            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        rootReference.child( "chat_conversations" ).child( currentUser.getUid() ).addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.hasChild(friendUserId)){

                    Map chatConversationAddMap = new HashMap();
                    chatConversationAddMap.put("seen", false);
                    chatConversationAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatConversationUserMap = new HashMap();
                    chatConversationUserMap.put("chat_conversations/"+ currentUserId + "/" + friendUserId, chatConversationAddMap);
                    chatConversationUserMap.put("chat_conversations/"+ friendUserId + "/" + currentUserId, chatConversationAddMap);

                    rootReference.updateChildren(chatConversationUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.d("ERROR_FULLTOPIA", databaseError.getMessage());
                            }
                        }
                    });

                }
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        messageView = (RecyclerView) findViewById(R.id.recycler_view_messages);

        rootReference.child( "chat_messages" ).child( currentUser.getUid() ).orderByChild( friendUserId ).addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {

/*                ChatMessage currentMessage;
                for (DataSnapshot currentUserData : dataSnapshot.getChildren()){

                    currentMessage = currentUserData.getValue(ChatMessage.class);
                    messageList.add(currentMessage);
                }*/

/*                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getLayoutInflater()) {
                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };

                RecyclerViewMessagesAdapter eventAdapter = new RecyclerViewMessagesAdapter(getActivity().getLayoutInflater(), userList);
                messageView.setLayoutManager(linearLayoutManager);
                messageView.setHasFixedSize(true);
                messageView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                messageView.setAdapter(eventAdapter);*/

            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }


        });


        editTextInput = (EditText) findViewById(R.id.chat_input_message);
        sendMessageButton = (ImageButton) findViewById(R.id.chat_send_button);

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });



    }

    private void sendMessage() {

        String message = editTextInput.getText().toString();

        if(!TextUtils.isEmpty( message )){
            String currentUserMessageRef = "chat_messages/" + currentUserId + "/" +friendUserId;
            String friendMessageRef = "chat_messages/" + friendUserId + "/" +currentUserId;

            DatabaseReference newMessagePush = rootReference.child("chat_messages").child(currentUserId).child(friendUserId).push();
            String newMessageId = newMessagePush.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("timestamp", ServerValue.TIMESTAMP);
            messageMap.put("owner", currentUserId);

            Map messageUserMap = new HashMap();
            messageUserMap.put( currentUserMessageRef + "/" +newMessageId, messageMap);
            messageUserMap.put( friendMessageRef + "/" +newMessageId, messageMap);


            rootReference.updateChildren( messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Log.d("ERROR_FULLTOPIA", databaseError.getMessage());
                    }

                    Notification newNotification = new Notification();
                    newNotification.setTitle( "You have a new message" );
                    newNotification.setBody( "Message from "+ friendUserName);
                    newNotification.setIcon( "default" );
                    newNotification.setNotificationType( "chat" );
                    newNotification.setUserReceiverKey( friendUserId );
                    newNotification.setClickAction( "ch.androidapp.hevs.fulltopia.Modules.Message.ChatActivity.NOTIFICATION" );
                    DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference().child( "notifications" ).child( friendUserId ).push();
                    notificationReference.setValue( newNotification );

                    editTextInput.setText("");

                }
            });

        }
    }


}
