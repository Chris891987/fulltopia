package ch.androidapp.hevs.fulltopia.Adapter;

import android.animation.ObjectAnimator;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

/**
 * Created by Christian-Lenovo on 29.08.2017.
 * Adapter for displaying Agenda - Events Past and futur
 */

public class RecyclerViewAgendaAdapter extends RecyclerView.Adapter<RecyclerViewAgendaAdapter.PostsViewHolder> {
    private LayoutInflater inflater;
    private List<Event> events;
    private ImageButton moreButton;
    private boolean expanded = false;
    private ObjectAnimator anim1;
    private ObjectAnimator anim2;
    private StorageReference mStorage;

    public RecyclerViewAgendaAdapter(LayoutInflater inflater, List<Event> events) {
        this.inflater = inflater;
        this.events = events;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemPost = inflater.inflate(R.layout.list_events, parent, false);
        PostsViewHolder postsViewHolder = new PostsViewHolder( itemPost);
        postsViewHolder.view = itemPost;
        postsViewHolder.eventName = (TextView) itemPost.findViewById(R.id.eventName);
        postsViewHolder.eventDate = (TextView) itemPost.findViewById(R.id.eventDate);
        moreButton = (ImageButton) itemPost.findViewById(R.id.moreButton);

        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(final PostsViewHolder holder, final int position) {
        Event event = events.get(position);
        holder.eventName.setText(event.getName());
        holder.eventDate.setText((CharSequence) event.getStartDate());
        holder.position = position;

        //Recovering the image stored on firebase
        mStorage = FirebaseStorage.getInstance().getReference("pictures/events/"+event.getId() );
        mStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                Glide.with(inflater.getContext())
                        .load( uri.toString())
                        .into(holder.thumbnail);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

                Glide.with(inflater.getContext())
                        .load( R.drawable.no_image )
                        .into(holder.thumbnail);
            }
        });


    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public static class PostsViewHolder extends RecyclerView.ViewHolder {
        protected View view;
        protected TextView eventName, eventDate;
        protected int position;
        protected CircleImageView thumbnail ;

        public PostsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.eventName = ( TextView ) view.findViewById( R.id.eventName );
            this.eventDate = ( TextView ) view.findViewById( R.id.eventDate );
            this.thumbnail = ( CircleImageView ) view.findViewById( R.id.chat_single_icon );
        }
    }
}
