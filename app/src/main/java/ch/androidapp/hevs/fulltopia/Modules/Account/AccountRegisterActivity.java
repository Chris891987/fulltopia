package ch.androidapp.hevs.fulltopia.Modules.Account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.SetupStepperActivity;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

/**
 * Register user page
 */
public class AccountRegisterActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference database;

    private TextInputLayout inputLayoutUserName;
    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private Button buttonSave;

    private Toolbar toolBar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );
        setContentView( R.layout.account_register_activity );
        FulltopiaUtils.setContext( this );

        toolBar = (Toolbar) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( "Create Account" );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        progressDialog = new ProgressDialog( this );
        firebaseAuth = FirebaseAuth.getInstance();

        inputLayoutUserName = (TextInputLayout) findViewById( R.id.user_register_input_user_name );
        inputLayoutEmail = (TextInputLayout) findViewById( R.id.stepper_sub_category );
        inputLayoutPassword = (TextInputLayout) findViewById( R.id.user_login_input_password );
        buttonSave = (Button) findViewById( R.id.user_register_btn_save );

        //when the button is pressed check form and data
        buttonSave.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                    Boolean isFormValid = true;

                    if(!FulltopiaUtils.isInputTextFilled( inputLayoutUserName )){
                        isFormValid = false;
                    }
                    if(!FulltopiaUtils.isValidEmail( inputLayoutEmail )){
                        isFormValid = false;
                    }
                    if(!FulltopiaUtils.isValidPassword( inputLayoutPassword )){
                        isFormValid = false;
                    }
                    if( isFormValid ){
                        String inputUserNameValue = inputLayoutUserName.getEditText().getText().toString();
                        String inputEmailValue = inputLayoutEmail.getEditText().getText().toString();
                        String inputPasswordValue = inputLayoutPassword.getEditText().getText().toString();

                        progressDialog.setTitle( "Registering Account" );
                        progressDialog.setMessage( "Please wait while we create your account" );
                        progressDialog.setCanceledOnTouchOutside( false );
                        progressDialog.show();
                        //if check OK
                        registerUser(inputUserNameValue, inputEmailValue, inputPasswordValue);
                    }
            }
        } );
    }

    //registration in the database
    private void registerUser( final String username, String email, String password ) {

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            //get current user
                            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                            String currentUserId = currentUser.getUid();
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();

                            database = FirebaseDatabase.getInstance().getReference().child("users").child( currentUserId );

                            HashMap<String, String> newUserObject = new HashMap();
                            newUserObject.put("name", username);
                            newUserObject.put("status", "Hi there i'm using Fulltopia");
                            newUserObject.put("hasCompleteRegistration", "false");
                            newUserObject.put("profile_picture", "default");
                            newUserObject.put("profile_picture_thumbnail", "default");
                            newUserObject.put("deviceToken", deviceToken);

                            //data entry in firebase
                            database.setValue( newUserObject ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful()){
                                        progressDialog.dismiss();
                                        Intent startIntent = new Intent( AccountRegisterActivity.this, SetupStepperActivity.class );
                                        startActivity( startIntent );
                                        finish();
                                    }else{
                                        progressDialog.hide();
                                        Log.w("TIAGO", "signInWithEmail:failed", task.getException());
                                        Toast.makeText(AccountRegisterActivity.this, "Register failed", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        //error managment
                        }else{
                            progressDialog.hide();

                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                inputLayoutPassword.setError(getString(R.string.error_weak_password));
                                inputLayoutPassword.requestFocus();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                inputLayoutEmail.setError(getString(R.string.error_invalid_email));
                                inputLayoutEmail.requestFocus();
                            } catch(FirebaseAuthUserCollisionException e) {
                                inputLayoutEmail.setError(getString(R.string.error_invalid_email));
                                inputLayoutEmail.requestFocus();
                            } catch(Exception e) {
                                Log.w("TIAGO", "signInWithEmail:failed", task.getException());
                            }

                            Toast.makeText(AccountRegisterActivity.this, "Register failed",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    }

    //button back
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
