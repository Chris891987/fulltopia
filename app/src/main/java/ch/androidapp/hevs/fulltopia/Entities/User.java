package ch.androidapp.hevs.fulltopia.Entities;

import com.google.firebase.database.Exclude;

public class User {
    @Exclude
    private String key;
    private String name;
    private String status;
    private String profile_picture;
    private String profile_picture_thumbnail;
    private Boolean hasCompleteRegistration;
    private Location location;


    public User(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public Boolean getHasCompleteRegistration() {
        return hasCompleteRegistration;
    }

    public void setHasCompleteRegistration(String hasCompleteRegistration) {
        this.hasCompleteRegistration = Boolean.parseBoolean(hasCompleteRegistration);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getProfile_picture_thumbnail() {
        return profile_picture_thumbnail;
    }

    public void setProfile_picture_thumbnail(String profile_picture_thumbnail) {
        this.profile_picture_thumbnail = profile_picture_thumbnail;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", profile_picture='" + profile_picture + '\'' +
                ", profile_picture_thumbnail='" + profile_picture_thumbnail + '\'' +
                ", hasCompleteRegistration=" + hasCompleteRegistration +
                ", location=" + location +
                '}';
    }
}
