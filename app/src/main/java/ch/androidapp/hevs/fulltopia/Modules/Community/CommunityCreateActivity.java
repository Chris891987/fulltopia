package ch.androidapp.hevs.fulltopia.Modules.Community;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Adapter.StepperCategoriesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.Modules.Global.GPSTracker;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CommunityCreateActivity extends AppCompatActivity implements OnConnectionFailedListener, OnMapReadyCallback {

    private static final String TAG = CommunityCreateActivity.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private LatLng myPosition;
    private SupportMapFragment mapFragment;
    private PlaceAutocompleteFragment autocompleteFragment;
    private String myPlaceName;
    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private ProgressDialog progressDialog;
    private ImageView imageView;
    private TextInputLayout inputLayoutName;
    private TextInputLayout inputLayoutDescription;
    private Button buttonCreate;
    private Button btnOpenSearcher;
    private Community community;
    private Toolbar toolBar;
    private Uri uri;
    private String uid;
    private boolean isEditing = false;
    private ArrayList<Category> allCategories = new ArrayList<Category>();
    private DatabaseReference referenceCategories;
    private DatabaseReference referenceCatCom;
    private StepperCategoriesAdapter adapter;
    private RecyclerView categoriesRecyclerView;
    private HashMap<String, Boolean> oldCategories;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.community_create_activity );

        //Setting global class
        FulltopiaUtils.setContext( this );

        //Add toolbar
        toolBar = ( Toolbar ) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( R.string.create_community_title );
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        //Preparing process dialog
        progressDialog = new ProgressDialog( this );

        //Reference to database
        mDatabase = FirebaseDatabase.getInstance().getReference( "communities" );
        mStorage = FirebaseStorage.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();

        //Recover elements
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.community_create_mapView );
        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById( R.id.place_autocomplete_fragment );
        inputLayoutName = (TextInputLayout) findViewById( R.id.community_create_input_name );
        inputLayoutDescription = (TextInputLayout) findViewById( R.id.community_create_text_edit_description );
        buttonCreate = (Button) findViewById( R.id.community_create_button_create );
        imageView = (ImageView) findViewById( R.id.imageView );
        btnOpenSearcher = (Button) findViewById( R.id.btn_show_searcher );

        //Setting map
        mapFragment.getMapAsync( this );
        //REcoverng current user
        currentUser = firebaseAuth.getCurrentUser();

        //Set imageView to prevent refresh on switch
        if( uri == null )
            imageView.setImageResource( R.drawable.ic_image_black_120dp );
        else
            setImageSelector();

        //Remplir categories
        allCategories = new ArrayList<Category>();
        referenceCategories = FirebaseDatabase.getInstance().getReference().child( "categories" );
        referenceCategories.orderByChild( "childs" ).startAt( "" ).addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                for ( DataSnapshot currentCategory : dataSnapshot.getChildren() ) {
                    Category newCategory = new Category( currentCategory.getKey() );
                    for ( DataSnapshot currentCategoryItem : currentCategory.getChildren() ) {

                        switch ( currentCategoryItem.getKey() ) {
                            case "name":
                                newCategory.setName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "iconName":
                                newCategory.setIconName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "textColor":
                                newCategory.setTextColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "backgroundColor":
                                newCategory.setBackgroundColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            default:
                                break;
                        }
                    }
                    allCategories.add( newCategory );
                }
                //Preparing recycler view
                adapter = new StepperCategoriesAdapter( CommunityCreateActivity.this, allCategories );
                categoriesRecyclerView = (RecyclerView) findViewById( R.id.categoriesRecyclerView );
                categoriesRecyclerView.setHasFixedSize( true );
                categoriesRecyclerView.setAdapter( adapter );

                // Manage if portrait or landscape
                int col;
                if ( getResources().getConfiguration().orientation == 1 )
                    col = 2;// Portrait => 2 colums
                else
                    col = 3;// Landscape => 3 colums

                GridLayoutManager gridLayout = new GridLayoutManager(getApplication() ,col) {
                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };

                categoriesRecyclerView.setLayoutManager( gridLayout );

                //If mode modify recover data from the create_activity
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    isEditing = true;
                    getSupportActionBar().setTitle( R.string.modify_community );
                    String jsonMyObject = extras.getString("community");
                    community = new Gson().fromJson(jsonMyObject, Community.class);
                    inputLayoutName.getEditText().setText(community.getName());
                    inputLayoutDescription.getEditText().setText(community.getDescription());
                    myPosition = new LatLng ( community.getLocationLatitude(),  community.getLocationLongitude() );
                    myPlaceName =  community.getLocationName();
                    if(community.getCategories() != null) {
                        for (int i = 0; i < adapter.getItemCount(); i++) {
                            for (int j = 0; j < community.getCategories().size(); j++) {
                                if (community.getCategories().containsKey(adapter.getItem(i).getKey())) {
                                    adapter.setCheck(i, true);
                                }
                            }
                        }
                        oldCategories = new HashMap<String, Boolean>(community.getCategories());
                    }
                }
                //Update the adapter
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        } );


        // Setup Api google
        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .enableAutoManage( this, this )
                .build();

        //Imageview listener on click
        imageView.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType( "image/*" );
                intent.setAction( Intent.ACTION_GET_CONTENT );
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser( intent, getString( R.string.select_picture ) ), PICK_IMAGE_REQUEST);
            }
        });

        //Fragement search google
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected( Place place ) {
                myPlaceName = place.getName().toString();
                myPosition = place.getLatLng();
                setPlace();
            }
            @Override
            public void onError(Status status) {
                Log.d(TAG, "An error occurred: " + status);
            }
        });

        //Open fragment search location powered by google
        btnOpenSearcher.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                EditText searcher = (EditText)autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
                searcher.callOnClick();

            }
        } );


        //Button create on click listener
        buttonCreate.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Boolean isFormValid = true;
                //Check if form is valid
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutDescription )){
                    isFormValid = false;
                }
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutName )){
                    isFormValid = false;
                }

                if( isFormValid ){
                    //Showing rpocess dialgo
                    progressDialog.setTitle(R.string.community);
                    progressDialog.show();
                    progressDialog.setCanceledOnTouchOutside( false );

                    //Preparing data to send
                    String inputCommunityNameValue = inputLayoutName.getEditText().getText().toString();
                    String inputCommunityDescriptionValue = inputLayoutDescription.getEditText().getText().toString();

                    //Current date
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();

                    //Setting categories
                    HashMap<String, Boolean> categories = new HashMap<>();
                    for (int i=0; i<adapter.getItemCount();i++){
                        if(adapter.getItem(i).getChecked())
                            categories.put( adapter.getItem(i).getKey(),true );
                    }

                    //Gettind reference collection
                    referenceCatCom = FirebaseDatabase.getInstance().getReference().child( "categories_communities" );

                    //Create the community or update (else)
                    if(!isEditing){
                        //Create unique id
                        uid = mDatabase.child( "community" ).push().getKey();
                        //Setting the community
                        community = new Community();
                        community.setDescription( inputCommunityDescriptionValue )
                                .setOwner( currentUser.getUid() )
                                .setName( inputCommunityNameValue)
                                .setLocationLatitude( myPosition.latitude  )
                                .setLocationLongitude( myPosition.longitude  )
                                .setLocationName( myPlaceName )
                                .setCategories( categories )
                                .setCreatedAt( dateFormat.format(date) )
                                .setUpdateAt( dateFormat.format(date) );
                        mDatabase.child(uid).setValue( community );

                        //Iterate on the hashmap
                        Iterator it = categories.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            referenceCatCom.child(String.valueOf(pair.getKey())).child(uid).setValue(pair.getValue());
                            it.remove();
                        }
                    }
                    else{
                        uid = community.getId();
                        Map updateCom = new HashMap<String,String>();
                        updateCom.put("name",inputCommunityNameValue);
                        updateCom.put("locationLatitude",myPosition.latitude);
                        updateCom.put("locationLongitude",myPosition.longitude);
                        updateCom.put("locationName",myPlaceName);
                        updateCom.put("description",inputCommunityDescriptionValue);
                        updateCom.put("updateAt",dateFormat.format(date));
                        updateCom.put("categories",categories);
                        mDatabase.child(uid).updateChildren(updateCom);

                        //Removing olders categories
                        if(community.getCategories() != null ) {
                            Iterator it = oldCategories.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                referenceCatCom.child(String.valueOf(pair.getKey())).child(uid).removeValue();
                                it.remove();
                            }
                        }
                        //Adding new categories
                        Iterator it2 = categories.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry pair2 = (Map.Entry)it2.next();
                            referenceCatCom.child(String.valueOf(pair2.getKey())).child(uid).setValue(pair2.getValue());
                            it2.remove();
                        }
                    }

                    //Upload the image on the server
                    StorageReference filePath = mStorage.child( "pictures/communities/"+uid);
                    if(uri!=null){
                        //Uploading image from the gallery
                        filePath.putFile( uri ).addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess( UploadTask.TaskSnapshot taskSnapshot ) {
                                mDatabase.child(uid).child( "urlDefault" ).setValue(false);
                                progressDialog.hide();
                                Intent intent = new Intent(CommunityCreateActivity.this, MenuActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }).addOnFailureListener( new OnFailureListener() {
                            @Override
                            public void onFailure( @NonNull Exception exception ) {
                                //if the upload is not successfull
                                //hiding the progress dialog
                                progressDialog.dismiss();
                                //and displaying error message
                                Toast.makeText( getApplicationContext(), "Error on uploading the picture", Toast.LENGTH_LONG ).show();
                            }
                        })
                        .addOnProgressListener( new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress( UploadTask.TaskSnapshot taskSnapshot ) {
                                //calculating progress percentage
                                @SuppressWarnings("VisibleForTests")
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //displaying percentage in progress dialog
                                if(isEditing) {
                                    progressDialog.setMessage(getString(R.string.updating_community) + " " + ((int) progress) + " % ... ");
                                }
                                else {
                                    progressDialog.setMessage(getString(R.string.creating_community) + " " + ((int) progress) + " % ... ");
                                }
                            }
                        });
                    }else{
                        if(isEditing){
                            //On editing not new image was selected
                            filePath.delete();
                        }
                        mDatabase.child(uid).child( "urlDefault" ).setValue(true);
                        progressDialog.hide();
                        Intent intent = new Intent(CommunityCreateActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        } );
    }

    private void setPlace() {
        //Update place on map
        mMap.addMarker(new MarkerOptions().position( myPosition ));
        mMap.moveCamera(CameraUpdateFactory.newLatLng( myPosition ));
        mMap.setMinZoomPreference(14.0f);
    }

    //Set Image selector
    private void setImageSelector() {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap( getContentResolver(), uri );
            imageView.setBackground( null );
            imageView.setImageBitmap( bitmap );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    //Set image view after pick from gallery
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            setImageSelector();
        }
    }

    //Prepare google map
    @Override
    public void onMapReady( GoogleMap googleMap ) {
        mMap = googleMap;
        if(!isEditing) {
            //Recovering the position
            GPSTracker gps = new GPSTracker(this);
            if (gps.canGetLocation()) {
                myPosition = new LatLng(gps.getLatitude(), gps.getLongitude());
            } else {
                gps.showSettingsAlert();
                myPosition = new LatLng(0, 0);
            }
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(myPosition.latitude, myPosition.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if(addresses.size() > 0){
                    myPlaceName = addresses.get(0).getLocality();
                }else{
                    myPlaceName = "";
                }
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
        setPlace();
    }

    //Connection to google service failed
    @Override
    public void onConnectionFailed( @NonNull ConnectionResult result ) {
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    //Click on elements on toolbar
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(CommunityCreateActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        //Save actual state of the application
        outState.putString( "inputLayoutName", inputLayoutName.getEditText().getText().toString() );
        outState.putString( "inputLayoutDescription", inputLayoutDescription.getEditText().getText().toString() );
        if(uri != null)
            outState.putString( "uri", uri.toString() );
        else
            outState.putString( "uri", "" );
        if(myPosition != null){
            outState.putParcelable( "position", myPosition );
            outState.putString( "namePlace", myPlaceName );
        }
        super.onSaveInstanceState( outState );
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState ) {
        //Recover state of the application
        if (savedInstanceState != null) {
            String inputLayoutNameValue = savedInstanceState.getString( "inputLayoutName" );
            String inputLayoutDescriptionValue = savedInstanceState.getString( "inputLayoutDescription" );
            uri = Uri.parse( savedInstanceState.getString( "uri" ) );
            myPosition = savedInstanceState.getParcelable( "position" );
            myPlaceName = savedInstanceState.getString( "namePlace" );

            inputLayoutName.getEditText().setText( inputLayoutNameValue );
            inputLayoutDescription.getEditText().setText( inputLayoutDescriptionValue );

            setImageSelector();
        }
        super.onRestoreInstanceState( savedInstanceState );
    }


}
