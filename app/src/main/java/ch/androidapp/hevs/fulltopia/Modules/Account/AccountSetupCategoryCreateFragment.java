package ch.androidapp.hevs.fulltopia.Modules.Account;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.HashMap;

/**
 * creation of a category
 */
public class AccountSetupCategoryCreateFragment extends Fragment implements Step {
    private TextInputLayout inputCategory;
    private TextInputLayout inputSubCategory;
    private Button buttonLogin;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference referenceCategories;
    private DatabaseReference referenceCategory;
    private DatabaseReference referenceSubCategory;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate( R.layout.account_setup_category_create_fragment, container, false);
        FulltopiaUtils.setContext( getContext() );

        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        String currentUserId = currentUser.getUid();

        inputCategory = (TextInputLayout) v.findViewById( R.id.stepper_category );
        inputSubCategory = (TextInputLayout) v.findViewById( R.id.stepper_sub_category );
        buttonLogin = (Button) v.findViewById( R.id.user_btn_login );

        buttonLogin.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Boolean isFormValid = true;

                if(!FulltopiaUtils.isInputTextFilled( inputCategory )){
                    isFormValid = false;
                }

                if(!FulltopiaUtils.isInputTextFilled( inputSubCategory )){
                    isFormValid = false;
                }

                if( isFormValid ){
                    referenceCategories = FirebaseDatabase.getInstance().getReference().child("categories");




                    referenceCategories.orderByChild("name").equalTo(inputCategory.getEditText().getText().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(!dataSnapshot.exists()){

                                referenceCategory = referenceCategories.push();
                                HashMap<String, String> newCategoryObject = new HashMap();
                                newCategoryObject.put("name", inputCategory.getEditText().getText().toString());
                                newCategoryObject.put("parent", null);
                                newCategoryObject.put("childs", null);
                                newCategoryObject.put("icon", null);
                                newCategoryObject.put("backgroundColor", null);
                                newCategoryObject.put("iconColor", null);

                                //Use the new reference to add the data
                                referenceCategory.setValue(newCategoryObject).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText(getActivity(), "Category added with success",
                                                    Toast.LENGTH_LONG).show();
                                        }else{
                                            Toast.makeText(getActivity(), "CATEGORY NOT ADDED",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }else{

                                for (DataSnapshot results: dataSnapshot.getChildren()) {
                                    referenceCategory = referenceCategories.child( results.getKey().toString() ).getRef();
                                }
                            }

                            referenceSubCategory = referenceCategories.push();
                            HashMap<String, String> newCategoryObject = new HashMap();
                            newCategoryObject.put("name", inputSubCategory.getEditText().getText().toString());
                            newCategoryObject.put("parent", referenceCategory.getKey().toString());

                            //Register in firebase
                            referenceSubCategory.setValue( newCategoryObject ).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(getActivity(), "SUB added with success",
                                                Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(getActivity(), "SUB NOT ADDED",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                            //Use the new reference to add the data
                            referenceCategory.child( "childs" ).child(referenceSubCategory.getKey()).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(getActivity(), "Category added with success",
                                                Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(getActivity(), "CATEGORY NOT ADDED",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }

                        @Override
                        public void onCancelled( DatabaseError databaseError ) {

                        }
                    });
                }
            }
        } );
        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

}