package ch.androidapp.hevs.fulltopia.Adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureChild;
import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureParent;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jacques on 05.09.2017.
 *  Adapter for displaying the search

 */

public class RecyclerViewSearchAdapter extends RecyclerView.Adapter<RecyclerViewSearchAdapter.SearchViewHolder> {
    //private LayoutInflater inflater;
    private List<StructureParent> searchResults;
    private Context mContext;

    private ImageButton moreButton;
    private boolean expanded = false;
    private ObjectAnimator animBT;
    private ObjectAnimator animRV;


    public RecyclerViewSearchAdapter(Context context, List<StructureParent> searchResults) {
        this.searchResults = searchResults;
        this.mContext = context;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        if(!(this.searchResults.isEmpty())) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_list, null);
            SearchViewHolder viewHolder = new SearchViewHolder(view);

            // viewHolder.categoryName = (TextView) view.findViewById(R.id.categoryName);
            //viewHolder.categoryImage = (CircleImageView) view.findViewById(R.id.img_thumbnail);
            //moreButton = (ImageButton) view.findViewById(R.id.moreButton);

            return viewHolder;

        }
        return null;
    }

    @Override
    public void onBindViewHolder(final SearchViewHolder holder,  int position) {
        final StructureParent sp = searchResults.get(position);
        final String title = sp.getName();
        final List<StructureChild> sChilds = sp.getChilds();
        final String countFoundItem = Integer.toString(sChilds.size());
        final String colorBackGround = sp.getBackgroundColor();

        holder.categoryName.setText(title);
        holder.countFoundItem.setText(countFoundItem);
        holder.categoryImage.setColorFilter( Color.parseColor( "#FFFFFF" ) );

        holder.relativeLayout.setBackgroundColor( Color.parseColor( colorBackGround ) );


        /*holder.categoryImage.setImageDrawable(
                new IconicsDrawable( inflater.getContext(), category.getIconName() ).sizeDp( 16 ).color( Color.parseColor( category.getTextColor() ) )
        );*/

        RecyclerViewSearchChildAdapter recyclerviewChilds = new RecyclerViewSearchChildAdapter(mContext, sChilds);
        holder.recycler_view_childs.setHasFixedSize(true);
        holder.recycler_view_childs.setAdapter(recyclerviewChilds);
        holder.recycler_view_childs.setNestedScrollingEnabled(false);

        holder.recycler_view_childs.setLayoutManager( new LinearLayoutManager( mContext, LinearLayoutManager.VERTICAL, false ) );

        //Si not data was found hide more button
        if(sChilds.size()==0)
            holder.moreButton.setVisibility(View.INVISIBLE);
        else
            holder.moreButton.setVisibility(View.VISIBLE);

        holder.moreButton.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                if(!expanded) {
                    animRV = ObjectAnimator.ofInt(
                            holder.recycler_view_childs,
                            "visibility",
                            View.GONE);
                    animBT = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            0,
                            180);
                    expanded=true;
                }
                else{
                    animRV = ObjectAnimator.ofInt(
                            holder.recycler_view_childs,
                            "visibility",
                            View.VISIBLE);
                    animBT = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            180,
                            0);
                    expanded=false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(animRV, animBT);
                animatorSet.setDuration(100);
                animatorSet.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    public static class SearchViewHolder extends RecyclerView.ViewHolder {

        protected TextView categoryName;
        protected TextView countFoundItem;
        protected CircleImageView categoryImage;
        protected RecyclerView recycler_view_childs;
        protected RelativeLayout relativeLayout;
        protected ImageButton moreButton;


        public SearchViewHolder(View itemView) {
            super( itemView );
            this.categoryName = (TextView) itemView.findViewById(R.id.categoryName);
            this.countFoundItem = (TextView) itemView.findViewById(R.id.searchTextViewNbrResults);
            this.categoryImage = (CircleImageView) itemView.findViewById(R.id.chat_single_icon);
            this.recycler_view_childs = (RecyclerView) itemView.findViewById(R.id.recycler_view_childs_result);
            this.relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_layout_search_result);
            this.moreButton = (ImageButton) itemView.findViewById(R.id.moreButton);
        }

    }
}
