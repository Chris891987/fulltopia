package ch.androidapp.hevs.fulltopia.Modules.Event;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.MediaStoreSignature;
import com.bumptech.glide.signature.StringSignature;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ch.androidapp.hevs.fulltopia.Adapter.StepperCategoriesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Modules.Global.DatePickerFragment;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.Modules.Global.GPSTracker;
import ch.androidapp.hevs.fulltopia.Modules.Global.MenuActivity;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Christian-Lenovo on 04.09.2017.
 */

public class EventCreateActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private static final String TAG = EventCreateActivity.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private LatLng myPosition;
    private SupportMapFragment mapFragment;
    private PlaceAutocompleteFragment autocompleteFragment;
    private String myPlaceName;
    private DatabaseReference referenceCatEvent;
    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private ProgressDialog progressDialog;
    private ImageView imageView;
    private TextInputLayout inputLayoutName;
    private TextInputLayout inputLayoutDescription;
    private TextInputLayout inputLayoutMaxMembers;
    private TextInputLayout inputLayoutMinMembers;
    private Button btnCreate;
    private Button btnOpenSearcher;
    private Button btnSetStartDate;
    private Button btnSetEndDate;
    private TextView textViewStartDate;
    private TextView textViewEndDate;
    private TextView textViewErrorDate;
    private Event event;
    private Toolbar toolBar;
    private Uri uri;
    private String uid;
    private boolean isEditing = false;
    private ArrayList<Category> allCategories = new ArrayList<Category>();
    private DatabaseReference referenceCategories;
    private StepperCategoriesAdapter adapter;
    private RecyclerView categoriesRecyclerView;
    private HashMap<String, Boolean> oldCategories;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.event_create_activity );

        //Setting global class
        FulltopiaUtils.setContext( this );

        //Add toolbar
        toolBar = ( Toolbar ) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        getSupportActionBar().setTitle( R.string.create_new_event );
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        //Preparing process dialog
        progressDialog = new ProgressDialog( this );

        //Reference to database
        mDatabase = FirebaseDatabase.getInstance().getReference( "events" );
        mStorage = FirebaseStorage.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();

        //Recover elements
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById( R.id.event_create_mapView );
        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById( R.id.place_autocomplete_fragment );
        inputLayoutName = (TextInputLayout) findViewById( R.id.event_create_input_name );
        inputLayoutDescription = (TextInputLayout) findViewById( R.id.event_create_text_edit_description );
        inputLayoutMaxMembers = (TextInputLayout) findViewById( R.id.event_create_text_edit_max_member );
        inputLayoutMinMembers = (TextInputLayout) findViewById( R.id.event_create_text_edit_min_member );
        btnCreate = (Button) findViewById( R.id.event_create_button_create );
        imageView = (ImageView) findViewById( R.id.imageView );
        btnSetStartDate = (Button) findViewById( R.id.btn_set_start_date );
        btnSetEndDate = (Button) findViewById( R.id.btn_set_end_date );
        btnOpenSearcher = (Button) findViewById( R.id.btn_show_searcher );
        textViewStartDate = (TextView) findViewById( R.id.textview_set_start_date );
        textViewEndDate = (TextView) findViewById( R.id.textview_set_end_date );
        textViewErrorDate = (TextView) findViewById( R.id.textview_error_date );

        //Setting map
        mapFragment.getMapAsync( this );
        //Recovering current user
        currentUser = firebaseAuth.getCurrentUser();

        //Set imageView to prevent refresh on switch
        if( uri == null )
            imageView.setImageResource( R.drawable.ic_image_black_120dp );
        else
            setImageSelector();

        //Remplir categories
        allCategories = new ArrayList<Category>();
        referenceCategories = FirebaseDatabase.getInstance().getReference().child( "categories" );
        referenceCategories.orderByChild( "childs" ).startAt( "" ).addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                for ( DataSnapshot currentCategory : dataSnapshot.getChildren() ) {
                    Category newCategory = new Category( currentCategory.getKey() );
                    for ( DataSnapshot currentCategoryItem : currentCategory.getChildren() ) {

                        switch ( currentCategoryItem.getKey() ) {
                            case "name":
                                newCategory.setName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "iconName":
                                newCategory.setIconName( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "textColor":
                                newCategory.setTextColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            case "backgroundColor":
                                newCategory.setBackgroundColor( currentCategoryItem.getValue( String.class ) );
                                break;
                            default:
                                break;
                        }
                    }
                    allCategories.add( newCategory );
                }
                //Preparing recycler view
                adapter = new StepperCategoriesAdapter( EventCreateActivity.this, allCategories );
                categoriesRecyclerView = (RecyclerView) findViewById( R.id.categoriesRecyclerView );
                categoriesRecyclerView.setHasFixedSize( true );
                categoriesRecyclerView.setAdapter( adapter );

                // Manage if portrait or landscape
                int col;
                if ( getResources().getConfiguration().orientation == 1 )
                    col = 2;// Portrait => 2 colums
                else
                    col = 3;// Landscape => 3 colums

                GridLayoutManager gridLayout = new GridLayoutManager(getApplication() ,col) {
                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };
                categoriesRecyclerView.setLayoutManager( gridLayout );
                //If mode modify recover data from create_activity
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    isEditing = true;
                    getSupportActionBar().setTitle( R.string.modify_event );
                    String jsonMyObject = extras.getString("event");
                    event = new Gson().fromJson(jsonMyObject, Event.class);
                    inputLayoutName.getEditText().setText(event.getName());
                    inputLayoutDescription.getEditText().setText(event.getDescription());
                    textViewStartDate.append(event.getStartDate());
                    textViewEndDate.append(event.getEndDate());
                    inputLayoutMinMembers.getEditText().setText(event.getMinPartecipants().toString());
                    inputLayoutMaxMembers.getEditText().setText(event.getMaxPartecipants().toString());
                    myPosition = new LatLng ( event.getLocationLatitude(),   event.getLocationLongitude() );
                    myPlaceName =  event.getLocationName();
                    if(event.getCategories() != null) {
                        for (int i = 0; i < adapter.getItemCount(); i++) {
                            for (int j = 0; j < event.getCategories().size(); j++) {
                                if (event.getCategories().containsKey(adapter.getItem(i).getKey())) {
                                    adapter.setCheck(i, true);
                                }
                            }
                        }
                        oldCategories = new HashMap<String, Boolean>(event.getCategories());
                    }
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        } );

        // Setup Api google
        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .enableAutoManage( this, this )
                .build();

        //Imageview listener on click
        imageView.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType( "image/*" );
                intent.setAction( Intent.ACTION_GET_CONTENT );
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser( intent, getString( R.string.select_picture ) ), PICK_IMAGE_REQUEST);
            }
        });

        //Fragement search google
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected( Place place ) {
                myPlaceName = place.getName().toString();
                myPosition = place.getLatLng();
                setPlace();
            }
            @Override
            public void onError(Status status) {
                Log.d(TAG, "An error occurred: " + status);
            }
        });

        //Open fragment search location powered by google
        btnOpenSearcher.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                EditText searcher = (EditText)autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
                searcher.callOnClick();

            }
        } );

        //Open fragment date picker to set start date
        btnSetStartDate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setFlag(DatePickerFragment.FLAG_START_DATE);
                datePickerFragment.show(getFragmentManager(), "datePicker");
            }
        } );

        //Open fragment date picker to set end date
        btnSetEndDate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                DatePickerFragment datePickerFragment2 = new DatePickerFragment();
                datePickerFragment2.setFlag(DatePickerFragment.FLAG_END_DATE);
                datePickerFragment2.show(getFragmentManager(), "datePicker");
            }
        } );

        //Button create on click listener
        btnCreate.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {
                Boolean isFormValid = true;
                //Check the form
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutDescription )){
                    isFormValid = false;
                }
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutName )){
                    isFormValid = false;
                }
                if(!FulltopiaUtils.isDateEndInFutur( textViewStartDate, textViewEndDate, textViewErrorDate )){
                    isFormValid = false;
                }
                if(!FulltopiaUtils.isInputTextFilled( inputLayoutMinMembers )){
                    isFormValid = false;
                }
                else if(!FulltopiaUtils.isEqualZero(inputLayoutMinMembers, inputLayoutMaxMembers)) {
                    if (!FulltopiaUtils.maxIsGreater(inputLayoutMinMembers, inputLayoutMaxMembers)) {
                        isFormValid = false;
                    }
                }
                if( isFormValid ){
                    //Showing rpocess dialgo
                    progressDialog.setTitle(R.string.event);
                    progressDialog.show();
                    progressDialog.setCanceledOnTouchOutside( false );
                    //Preparing data to send
                    String inputCommunityNameValue = inputLayoutName.getEditText().getText().toString();
                    String inputCommunityDescriptionValue = inputLayoutDescription.getEditText().getText().toString();

                    //Current date
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();

                    //Setting categories
                    HashMap<String,Boolean> categories = new HashMap<>();
                    for (int i=0; i<adapter.getItemCount();i++){
                        if(adapter.getItem(i).getChecked())
                            categories.put( adapter.getItem(i).getKey(),true );
                    }

                    referenceCatEvent = FirebaseDatabase.getInstance().getReference().child( "categories_events" );

                    //Create or modify the event?
                    if(!isEditing){
                        //Create the event
                        //Create unique id
                        uid = mDatabase.child( "events" ).push().getKey();
                        //Setting the event
                        Event event = new Event();
                        event.setDescription( inputCommunityDescriptionValue )
                                .setOwner( currentUser.getUid() )
                                .setName( inputCommunityNameValue)
                                .setLocationLatitude(  myPosition.latitude  )
                                .setLocationLongitude(  myPosition.longitude  )
                                .setLocationName( myPlaceName )
                                .setCategories( categories )
                                .setCreatedAt( dateFormat.format(date) )
                                .setUpdateAt( dateFormat.format(date) );
                        event.setEndDate(textViewEndDate.getText().toString())
                                .setStartDate(textViewStartDate.getText().toString())
                                .setMaxPartecipants(Integer.parseInt(inputLayoutMaxMembers.getEditText().getText().toString()))
                                .setMinPartecipants(Integer.parseInt(inputLayoutMinMembers.getEditText().getText().toString()));
                        mDatabase.child(uid).setValue(event);

                        //Add related categorie to table categories_events
                        Iterator it = categories.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            referenceCatEvent.child(String.valueOf(pair.getKey())).child(uid).setValue(pair.getValue());
                            it.remove();
                        }
                    }
                    else{

                        //Update the event
                        uid = event.getId();
                        Map updateCom = new HashMap<String,String>();
                        updateCom.put("name",inputCommunityNameValue);
                        updateCom.put("locationLatitude",myPosition.latitude);
                        updateCom.put("locationLongitude",myPosition.longitude);
                        updateCom.put("locationName",myPlaceName);
                        updateCom.put("description",inputCommunityDescriptionValue);
                        updateCom.put("updateAt",dateFormat.format(date));
                        updateCom.put("categories",categories);
                        updateCom.put("startDate",textViewStartDate.getText().toString());
                        updateCom.put("endDate",textViewEndDate.getText().toString());
                        updateCom.put("minPartecipants",Integer.parseInt(inputLayoutMinMembers.getEditText().getText().toString()));
                        updateCom.put("maxPartecipants",Integer.parseInt(inputLayoutMaxMembers.getEditText().getText().toString()));
                        mDatabase.child(uid).updateChildren(updateCom);

                        //Removing olders categories
                        if(event.getCategories() != null ) {
                            Iterator it = oldCategories.entrySet().iterator();
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                referenceCatEvent.child(String.valueOf(pair.getKey())).child(uid).removeValue();
                                it.remove();
                            }
                        }
                        //Adding new categories
                        Iterator it2 = categories.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry pair2 = (Map.Entry)it2.next();
                            referenceCatEvent.child(String.valueOf(pair2.getKey())).child(uid).setValue(pair2.getValue());
                            it2.remove();
                        }
                    }

                    //Upload the picture
                    StorageReference filePath = mStorage.child( "pictures/events/"+uid);
                    //Image selected?
                    if(uri!=null){
                        //Uploading image from the gallery
                        filePath.putFile( uri ).addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess( UploadTask.TaskSnapshot taskSnapshot ) {
                                mDatabase.child(uid).child( "urlDefault" ).setValue(false);
                                progressDialog.hide();
                                Intent intent = new Intent(EventCreateActivity.this, MenuActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }).addOnFailureListener( new OnFailureListener() {
                            @Override
                            public void onFailure( @NonNull Exception exception ) {
                                //if the upload is not successfull
                                //hiding the progress dialog
                                progressDialog.dismiss();
                                //and displaying error message
                                Toast.makeText( getApplicationContext(), "Error on uploading the picture", Toast.LENGTH_LONG ).show();
                            }
                        }).addOnProgressListener( new OnProgressListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onProgress( UploadTask.TaskSnapshot taskSnapshot ) {
                                //calculating progress percentage
                                @SuppressWarnings("VisibleForTests")
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //displaying percentage in progress dialog
                                if(isEditing){
                                    progressDialog.setMessage( getString( R.string.updating_event ) + " " + ((int) progress) + " % ... " );
                                }
                                else{
                                    progressDialog.setMessage( getString( R.string.creating_event ) + " " + ((int) progress) + " % ... " );
                                }

                            }
                        });
                    }
                    else{
                        if(isEditing){
                            //On editing not new image was selected
                            filePath.delete();
                        }
                        mDatabase.child(uid).child( "urlDefault" ).setValue(true);
                        progressDialog.hide();
                        Intent intent = new Intent(EventCreateActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        } );
    }

    //Update place on map
    private void setPlace() {
        mMap.addMarker(new MarkerOptions().position( myPosition ));
        mMap.moveCamera(CameraUpdateFactory.newLatLng( myPosition ));
        mMap.setMinZoomPreference(14.0f);
    }

    //Set Image selector
    private void setImageSelector() {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap( getContentResolver(), uri );
            imageView.setBackground( null );
            imageView.setImageBitmap( bitmap );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    //Set image view after pick from gallery
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            setImageSelector();
        }
    }

    //Prepare google map
    @Override
    public void onMapReady( GoogleMap googleMap ) {
        mMap = googleMap;
        if(!isEditing) {
            //Recovering the position
            GPSTracker gps = new GPSTracker(this);
            if (gps.canGetLocation()) {
                myPosition = new LatLng(gps.getLatitude(), gps.getLongitude());
            } else {
                gps.showSettingsAlert();
                myPosition = new LatLng(0, 0);
            }
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                addresses = geocoder.getFromLocation(myPosition.latitude, myPosition.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if(addresses.size() > 0){
                    myPlaceName = addresses.get(0).getLocality();
                }else{
                    myPlaceName = "";
                }

            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
        setPlace();
    }

    //Connection to google service failed
    @Override
    public void onConnectionFailed( @NonNull ConnectionResult result ) {
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    //Click on elements on toolbar
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(EventCreateActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        //Save actual state of the application
        outState.putString( "inputLayoutName", inputLayoutName.getEditText().getText().toString() );
        outState.putString( "inputLayoutDescription", inputLayoutDescription.getEditText().getText().toString() );
        if(uri != null)
            outState.putString( "uri", uri.toString() );
        else
            outState.putString( "uri", "" );
        if(myPosition != null){
            outState.putParcelable( "position", myPosition );
            outState.putString( "namePlace", myPlaceName );
        }
        super.onSaveInstanceState( outState );
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState ) {
        //Recover state of the application
        if (savedInstanceState != null) {
            String inputLayoutNameValue = savedInstanceState.getString( "inputLayoutName" );
            String inputLayoutDescriptionValue = savedInstanceState.getString( "inputLayoutDescription" );
            uri = Uri.parse( savedInstanceState.getString( "uri" ) );
            myPosition = savedInstanceState.getParcelable( "position" );
            myPlaceName = savedInstanceState.getString( "namePlace" );

            inputLayoutName.getEditText().setText( inputLayoutNameValue );
            inputLayoutDescription.getEditText().setText( inputLayoutDescriptionValue );

            setImageSelector();
        }
        super.onRestoreInstanceState( savedInstanceState );
    }



}
