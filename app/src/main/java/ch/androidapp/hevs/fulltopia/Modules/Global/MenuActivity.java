package ch.androidapp.hevs.fulltopia.Modules.Global;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import ch.androidapp.hevs.fulltopia.Modules.Account.AccountProfileFragment;
import ch.androidapp.hevs.fulltopia.Modules.Agenda.AgendaFragment;
import ch.androidapp.hevs.fulltopia.Modules.Community.CommunityCreateActivity;
import ch.androidapp.hevs.fulltopia.Modules.DashBoard.DashboardFragment;
import ch.androidapp.hevs.fulltopia.Modules.Event.EventCreateActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.EditProfileStepperActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.Stepper.SetupStepperActivity;
import ch.androidapp.hevs.fulltopia.Modules.Message.MessageFragment;
import ch.androidapp.hevs.fulltopia.Modules.Search.SearchFragment;
import ch.androidapp.hevs.fulltopia.Modules.Settings.SettingsActivity;
import ch.androidapp.hevs.fulltopia.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MenuActivity extends AppCompatActivity {

    private Fragment selectedFragment = null;
    private Toolbar toolBar;
    private FirebaseAuth firebaseAuth;
    private Intent intent;
    private BottomNavigationView navigation;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private int idMenu ;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch ( item.getItemId() ) {
                case R.id.navigation_dashboard:
                    System.out.println(R.id.navigation_dashboard);
                    getSupportActionBar().setTitle( R.string.title_dashboard );
                    selectedFragment = DashboardFragment.newInstance();
                    break;

                case R.id.navigation_search:
                    getSupportActionBar().setTitle( R.string.title_search );
                    selectedFragment = new SearchFragment();
                    break;

                case R.id.navigation_agenda:
                    getSupportActionBar().setTitle( R.string.title_agenda );
                    selectedFragment = AgendaFragment.newInstance();
                    break;

                case R.id.navigation_messages:
                    getSupportActionBar().setTitle( R.string.title_message );
                    selectedFragment = MessageFragment.newInstance();
                    break;

                case R.id.navigation_profil:
                    getSupportActionBar().setTitle( R.string.title_account );
                    selectedFragment = AccountProfileFragment.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace( R.id.frame_layout, selectedFragment );
            transaction.commit();
            return true;
        }

    };

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.menu_activity );

        //Check internet connection
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo()==null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getString(R.string.title_internet_connection));
            alertDialogBuilder
                    .setMessage(getString(R.string.message_internet_fail))
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MenuActivity.this.finish();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        //Setting toolbar
        toolBar = (Toolbar) findViewById( R.id.app_bar_layout );
        setSupportActionBar( toolBar );
        //Manually displaying the first fragment - one time only
        getSupportActionBar().setTitle( R.string.title_dashboard );

        //Setting bottom navigation
        navigation = ( BottomNavigationView ) findViewById( R.id.navigation );
        navigation.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );

        //On create set the correct view
        switch ( FulltopiaUtils.idMenu ) {
            case R.id.navigation_dashboard:
                navigation.setSelectedItemId(R.id.navigation_dashboard);
                break;

            case R.id.navigation_search:
                navigation.setSelectedItemId(R.id.navigation_search);
                break;

            case R.id.navigation_agenda:
                navigation.setSelectedItemId(R.id.navigation_agenda);
                break;

            case R.id.navigation_messages:
                navigation.setSelectedItemId(R.id.navigation_messages);
                break;

            case R.id.navigation_profil:
                navigation.setSelectedItemId(R.id.navigation_profil);
                break;
            default:
                navigation.setSelectedItemId(R.id.navigation_dashboard);
        }

        //Prepare firebase
        firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged( @NonNull FirebaseAuth firebaseAuth ) {
                FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                //If not conneccted -> go to start
                if ( currentUser == null ) {
                    Intent startIntent = new Intent( MenuActivity.this, StartActivity.class );
                    startActivity( startIntent );
                    finish();
                }else{
                    String currentUserId = currentUser.getUid();
                    DatabaseReference hasCompleteRegistration = FirebaseDatabase.getInstance().getReference().child("users").child( currentUserId ).child("hasCompleteRegistration");
                    //Check if the registration is complete
                    hasCompleteRegistration.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if(!Boolean.parseBoolean( dataSnapshot.getValue(String.class) )){
                                Intent startIntent = new Intent( MenuActivity.this, SetupStepperActivity.class );
                                startActivity( startIntent );
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            Log.d("TITI", "Field hasCompleteRegistration  not found for this user");
                        }
                    });

                }
            }
        };


    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener( firebaseAuthListener );
    }

    @Override
    public void onStop() {
        super.onStop();
        if ( firebaseAuthListener != null ) {
            firebaseAuth.removeAuthStateListener( firebaseAuthListener );
        }
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ){
        switch( item.getItemId() ){
            //Manage menu on menuactivity
            case R.id.action_add_community:
                intent = new Intent( this, CommunityCreateActivity.class );
                startActivity( intent );
                return true;
            case R.id.action_add_event:
                intent = new Intent( this, EventCreateActivity.class );
                startActivity( intent );
                return true;
            case R.id.action_search:
                Toast.makeText( this, "This search", Toast.LENGTH_SHORT ).show();
                return true;
            case R.id.navigation_logout:
                Toast.makeText( this,"Logout...", Toast.LENGTH_SHORT ).show();
                FirebaseAuth.getInstance().signOut();
                return true;
            case R.id.navigation_settings:
                intent= new Intent( this, SettingsActivity.class );
                startActivity( intent );
                return true;
            case R.id.navigation_edit:
                intent = new Intent( this, EditProfileStepperActivity.class );
                startActivity( intent );
                return true;

        }
        return onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        FulltopiaUtils.idMenu = navigation.getSelectedItemId();
        super.onSaveInstanceState( outState );
    }

}
