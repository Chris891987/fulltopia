package ch.androidapp.hevs.fulltopia.Entities;

public class Category {

    private String key = "no-key";
    private String name = "";
    private String iconName = "faw-tag";
    private String backgroundColor = "#333333";
    private String textColor = "#FFFFFF";
    private Boolean isChecked = false;

    public Category() {
    }

    public Category( String key ) {
        this.key = key;
    }

    public Category( String key, String name, String iconName, String backgroundColor, String textColor,Boolean isChecked ) {
        this.key = key;
        this.name = name;
        this.iconName = iconName;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
        this.isChecked = isChecked;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName( String iconName ) {
        this.iconName = iconName;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor( String backgroundColor ) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor( String textColor ) {
        this.textColor = textColor;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked( Boolean checked ) {
        isChecked = checked;
    }


    public String getKey() {
        return key;
    }

    public void setKey( String key ) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Category{" +
                "key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", iconName='" + iconName + '\'' +
                ", backgroundColor='" + backgroundColor + '\'' +
                ", textColor='" + textColor + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
