package ch.androidapp.hevs.fulltopia.Modules.Global;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Rexy on 15.07.2017.
 */

public class FulltopiaUtils {

    private static Context context = null;
    public static boolean firstStart = false;
    public static String TYPE_EVENT= "event";
    public static String TYPE_COMMUNITY= "community";
    public static String TYPE_USER= "user";
    public static int idMenu;

    //Settings
    // Déclaration de variables globales
    public static final String KEY_PREF_LANGUAGE = "pref_language";
    public static final String KEY_PREF_THEME = "pref_theme";
    public static final String KEY_PREF_NOTIFICATION = "pref_notification";
    public static final String KEY_PREF_MESSAGES = "pref_messages";
    public static String notification;
    public static String theme;
    public static String messages;


    public static Context getContext() {
        return context;
    }
    public static void setContext( Context context ) {
        FulltopiaUtils.context = context;
    }

    //Check if the inpt is filled
    public final static boolean isInputTextFilled(TextInputLayout input ) {
        String inputValue = input.getEditText().getText().toString();
        input.setError( "" );
        if ( TextUtils.isEmpty( inputValue ) ) {
            input.setError( FulltopiaUtils.getContext().getString( R.string.app_field_required ) );
            input.requestFocus();
            return false;
        }
        return true;
    }

    //Check if the mail is valid
    public final static boolean isValidEmail( TextInputLayout input ) {
        String inputValue = input.getEditText().getText().toString();

        if ( FulltopiaUtils.isInputTextFilled( input ) ) {
            if ( android.util.Patterns.EMAIL_ADDRESS.matcher( inputValue ).matches() ) {
                return true;
            } else {
                input.setError( FulltopiaUtils.getContext().getString( R.string.app_email_required ) );
                input.requestFocus();
            }
        }
        return false;
    }

    //Check if the password is valid
    public final static boolean isValidPassword( TextInputLayout input ) {
        String inputValue = input.getEditText().getText().toString();

        if ( FulltopiaUtils.isInputTextFilled( input ) ) {
            if ( inputValue.length() < 6 ) {
                input.setError( FulltopiaUtils.getContext().getString( R.string.app_password_not_valid ) );
                input.requestFocus();
            } else {
                return true;
            }
        }
        return false;
    }

    //Check unlimited for partecipants
    public static boolean isEqualZero(TextInputLayout inputLayoutMinMembers,  TextInputLayout inputLayoutMaxMembers) {
        Integer inputMinValue = Integer.parseInt(inputLayoutMinMembers.getEditText().getText().toString());
        if ( inputMinValue ==  0) {
            inputLayoutMaxMembers.getEditText().setText("0");
            return true;
        }
        return false;
    }

    //Check if date is correted
    public static boolean isDateEndInFutur(TextView inputDateStart, TextView inputDateEnd, TextView inputErrorDate) {
        String dateStart = inputDateStart.getText().toString();
        String dateEnd = inputDateEnd.getText().toString();
        inputErrorDate.setText("");
        inputErrorDate.setVisibility(View.GONE);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try{
            Date start = dateFormat.parse(dateStart);
            Date end = dateFormat.parse(dateEnd);
            if ( start.before(end) ) {
                return true;
            }
        }
        catch (Exception ex){
            Log.e("Error parse date",ex.getStackTrace().toString());
        }
        inputErrorDate.setText("Dates filleds incorrectly");
        inputErrorDate.setVisibility(View.VISIBLE);
        return false;
    }

    //Check max and min partecipants
    public static boolean maxIsGreater(TextInputLayout inputLayoutMinMembers, TextInputLayout inputLayoutMaxMembers) {
        Integer inputMinValue = Integer.parseInt(inputLayoutMinMembers.getEditText().getText().toString());
        Integer inputMaxValue = Integer.parseInt(inputLayoutMaxMembers.getEditText().getText().toString());
        inputLayoutMinMembers.setError( " " );
        inputLayoutMaxMembers.setError( " " );
        if ( inputMaxValue <  inputMinValue) {
            inputLayoutMaxMembers.setError( FulltopiaUtils.getContext().getString( R.string.max_less_min ));
            return false;
        }
        return true;
    }

    public final static ArrayList<Category> getSearchableCategories(){
        ArrayList<Category> res = new ArrayList<Category>();
        res.add(new Category("100","Community", "faw-users", "#f39c12", "#FFFFFF",false));
        res.add(new Category("200","Events", "faw-calendar", "#27ae60", "#FFFFFF",false));
 /*       res.add(new Category("300","Material", "faw-envelope", "#555555", "#AAAAAA",false));
        res.add(new Category("301","Knowledge", "faw-envelope", "#666666", "#AAAAAA",false));
        res.add(new Category("302","Service", "faw-envelope", "#777777", "#AAAAAA",false));*/
        res.add(new Category("400","People", "faw-user-circle", "#3498db", "#FFFFFF",false));
        return res;
    }


    // Settings
    //Fonction qui défini les préférences de l'utilisateur
    public static void setPreferences(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        theme = sharedPreferences.getString(KEY_PREF_THEME, "");
        String language = sharedPreferences.getString(KEY_PREF_LANGUAGE, "");
        notification = sharedPreferences.getString(KEY_PREF_NOTIFICATION, "");
        messages = sharedPreferences.getString(KEY_PREF_MESSAGES, "");
        Locale myLocale = new Locale(language);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        //getTimeZone();
        config.locale = myLocale;

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
