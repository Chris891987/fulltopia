package ch.androidapp.hevs.fulltopia.Entities;

public class ChatMessage {

    private String message;
    private String seen;
    private String timestamp;
    private String owner;

    public ChatMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "message='" + message + '\'' +
                ", seen='" + seen + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }
}
