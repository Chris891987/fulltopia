package ch.androidapp.hevs.fulltopia.Adapter;

/**
 * Created by Christian-Lenovo on 22.08.2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Entities.AbstractObject;
import ch.androidapp.hevs.fulltopia.Modules.Community.CommunityViewActivity;
import ch.androidapp.hevs.fulltopia.Modules.Event.EventViewActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.R;

import java.util.ArrayList;
import java.util.Calendar;

public class SectionDashboardAdapter extends RecyclerView.Adapter<SectionDashboardAdapter.SingleItemRowHolder>{

    private ArrayList<AbstractObject> itemsList;
    private Context mContext;
    private StorageReference mStorage;

    public SectionDashboardAdapter(Context context, ArrayList<AbstractObject> itemsList ) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder( ViewGroup viewGroup, int i ) {
        View v = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.list_single_card, null );
        SingleItemRowHolder mh = new SingleItemRowHolder( v );
        return mh;
    }

    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, int i ) {

        AbstractObject singleAbstractObject = itemsList.get( i );

        holder.id = singleAbstractObject.getId();
        holder.itemTitle.setText( singleAbstractObject.getName() );
        holder.itemDescription.setText( singleAbstractObject.getDescription() );
        holder.urlDefault = singleAbstractObject.getUrlDefault();
        if( singleAbstractObject instanceof Event){
            holder.itemDate.setText( ((Event) singleAbstractObject).getStartDate());
            holder.type = FulltopiaUtils.TYPE_EVENT;
            mStorage = FirebaseStorage.getInstance().getReference("pictures/events/"+singleAbstractObject.getId() );
        }
        else {
            holder.itemDate.setText( "" );
            holder.type = FulltopiaUtils.TYPE_COMMUNITY;
            mStorage = FirebaseStorage.getInstance().getReference("pictures/communities/"+singleAbstractObject.getId() );
        }
        if(!holder.urlDefault){
            //Recovering the image stored on firebase
            mStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(mContext )
                            .load( uri.toString())
                            .into(holder.itemImage);
                }
            });
        }
        else{
            Glide.with(mContext )
                    .load( R.drawable.no_image)
                    .into(holder.itemImage);
        }
    }

    @Override
    public int getItemCount() {
        return ( null != itemsList ? itemsList.size() : 0 );
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected ImageView itemImage;
        protected TextView itemDescription;
        protected TextView itemDate;
        protected String type;
        protected String id;
        protected Boolean urlDefault;

        public SingleItemRowHolder( View view ) {
            super( view );

            this.itemTitle = ( TextView ) view.findViewById( R.id.itemTitle );
            this.itemDescription = ( TextView ) view.findViewById( R.id.itemDescription );
            this.itemImage = ( ImageView ) view.findViewById( R.id.itemImage );
            this.itemDate = ( TextView ) view.findViewById( R.id.itemDate );

            view.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick( View v ) {
                    if(type.equals(FulltopiaUtils.TYPE_COMMUNITY)){
                        Intent intent = new Intent(mContext, CommunityViewActivity.class);
                        intent.putExtra("id",id);
                        mContext.startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(mContext, EventViewActivity.class);
                        intent.putExtra("id",id);
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }
}
