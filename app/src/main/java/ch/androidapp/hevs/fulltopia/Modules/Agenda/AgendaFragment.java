package ch.androidapp.hevs.fulltopia.Modules.Agenda;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ch.androidapp.hevs.fulltopia.R;


public class AgendaFragment extends Fragment {

    private static AgendaFragment fragment = null;

    public static AgendaFragment newInstance() {
        fragment = new AgendaFragment();
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }

    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View myInflatedView = inflater.inflate(R.layout.message_fragment, container, false );
        TabLayout tabLayout = ( TabLayout) myInflatedView.findViewById(R.id.tab_layout );
        tabLayout.addTab( tabLayout.newTab().setText( R.string.past ) );
        tabLayout.addTab( tabLayout.newTab().setText( R.string.futur ) );
        tabLayout.setTabGravity( TabLayout.GRAVITY_FILL );

        final ViewPager viewPager = ( ViewPager) myInflatedView.findViewById(R.id.pager );
        final PagerAdapterAgenda adapter = new PagerAdapterAgenda( getFragmentManager(), tabLayout.getTabCount() );
        viewPager.setAdapter( adapter );
        viewPager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener( tabLayout ));
        tabLayout.addOnTabSelectedListener( new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected( TabLayout.Tab tab ) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected( TabLayout.Tab tab ) {

            }

            @Override
            public void onTabReselected( TabLayout.Tab tab ) {

            }
        });
        return myInflatedView;
    }

}
