package ch.androidapp.hevs.fulltopia.Entities.SearchStructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacques on 07.09.2017.
 */

public class StructureParent {
    private String name;
    private String backgroundColor;


    private List<StructureChild> childs= new ArrayList<StructureChild>();

    public StructureParent() {

    }
    public StructureParent(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StructureChild> getChilds() {
        return childs;
    }

    public void setChilds(List<StructureChild> childs) {
        this.childs = childs;
    }

    public void addChild(StructureChild sc){
        this.childs.add(sc);
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

}
