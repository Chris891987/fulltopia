package ch.androidapp.hevs.fulltopia.Adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import ch.androidapp.hevs.fulltopia.Entities.Post;
import ch.androidapp.hevs.fulltopia.R;
import java.util.List;

/**
 * Created by Christian-Lenovo on 29.08.2017.
 *  * Adapter for displaying Posts from users in communities or events.

 */

public class RecyclerViewPostAdapter extends RecyclerView.Adapter<RecyclerViewPostAdapter.PostsViewHolder> {
    private LayoutInflater inflater;
    private List<Post> posts;
    private ImageButton moreButton;
    private boolean expanded = false;
    private ObjectAnimator anim1;
    private ObjectAnimator anim2;

    public RecyclerViewPostAdapter(LayoutInflater inflater, List<Post> posts) {
        this.inflater = inflater;
        this.posts = posts;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemPost = inflater.inflate(R.layout.list_comments, parent, false);
        PostsViewHolder postsViewHolder = new PostsViewHolder( itemPost);
        postsViewHolder.view = itemPost;
        postsViewHolder.userName = (TextView) itemPost.findViewById(R.id.chat_single_message);
        postsViewHolder.messageText = (TextView) itemPost.findViewById(R.id.messageText);
        postsViewHolder.messageDate = (TextView) itemPost.findViewById(R.id.messageDate);
        moreButton = (ImageButton) itemPost.findViewById(R.id.moreButton);

        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(final PostsViewHolder holder, final int position) {
        Post post = posts.get(position);
        holder.userName.setText(post.getUser());
        holder.messageText.setText(post.getMessage());
        holder.messageDate.setText(post.getDate());
        holder.position = position;

        moreButton.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                if(!expanded) {
                    anim1 = ObjectAnimator.ofInt(
                            holder.messageText,
                            "maxLines",
                            100);
                    anim2 = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            0,
                            180);
                    expanded=true;
                }
                else{
                    anim1 = ObjectAnimator.ofInt(
                            holder.messageText,
                            "maxLines",
                            4);
                    anim2 = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            180,
                            0);
                    expanded=false;
                }

                //Animation for the expand
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(anim2, anim1);
                animatorSet.setDuration(1000);
                animatorSet.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public static class PostsViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView userName, messageText, messageDate;
        int position;

        public PostsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
        }
    }
}
