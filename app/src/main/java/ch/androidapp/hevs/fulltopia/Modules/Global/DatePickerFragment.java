package ch.androidapp.hevs.fulltopia.Modules.Global;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ch.androidapp.hevs.fulltopia.R;

/**
 * Created by Christian-Lenovo on 04.09.2017.
 *
 */

public class DatePickerFragment extends DialogFragment  implements
        DatePickerDialog.OnDateSetListener{

    public static final int FLAG_START_DATE = 0;
    public static final int FLAG_END_DATE = 1;
    private int flag = 0;
    private TextView textViewStartDate;
    private TextView textViewEndDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);

    }

    public void setFlag(int i) {
        flag = i;
    }
    //inserting dates by a Picker
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        //After the date picker we start the time picker
        if (flag == FLAG_START_DATE) {
            textViewStartDate = (TextView) getActivity().findViewById( R.id.textview_set_start_date );
            textViewStartDate.setText(view.getYear()+"/"+String.format("%02d",view.getMonth())+"/"+String.format("%02d",view.getDayOfMonth()));
            TimePickerFragment timePickerFragment = new TimePickerFragment();
            timePickerFragment.setFlag(TimePickerFragment.FLAG_START_TIME);
            timePickerFragment.show(getFragmentManager(),"TimePicker");
        } else if (flag == FLAG_END_DATE) {
            textViewEndDate = (TextView) getActivity().findViewById( R.id.textview_set_end_date );
            textViewEndDate.setText(view.getYear()+"/"+String.format("%02d",view.getMonth())+"/"+String.format("%02d",view.getDayOfMonth()));
            TimePickerFragment timePickerFragment = new TimePickerFragment();
            timePickerFragment.setFlag(TimePickerFragment.FLAG_END_TIME);
            timePickerFragment.show(getFragmentManager(),"TimePicker");
        }
    }


}
