package ch.androidapp.hevs.fulltopia.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ch.androidapp.hevs.fulltopia.Entities.User;
import ch.androidapp.hevs.fulltopia.Modules.Message.ChatActivity;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewMessagesAdapter extends RecyclerView.Adapter<RecyclerViewMessagesAdapter.PostsViewHolder> {
    private LayoutInflater inflater;
    private List<User> users;

    public RecyclerViewMessagesAdapter(LayoutInflater inflater, List<User> users) {
        this.inflater = inflater;
        this.users = users;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemPost = inflater.inflate(R.layout.list_messages, parent, false);
        PostsViewHolder postsViewHolder = new PostsViewHolder( itemPost);
        postsViewHolder.view = itemPost;
        postsViewHolder.userName = (TextView) itemPost.findViewById(R.id.chat_single_message);
        postsViewHolder.messageDate = (TextView) itemPost.findViewById(R.id.messageDate);
        postsViewHolder.messageText = (TextView) itemPost.findViewById(R.id.messageText);
        postsViewHolder.profilePicture = (CircleImageView) itemPost.findViewById(R.id.chat_single_icon);


        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(final PostsViewHolder holder, final int position) {
        final User user  = users.get(position);
        holder.userName.setText(user.getName());
        holder.messageText.setText(user.getStatus());


        if(!user.getProfile_picture_thumbnail().equals("default")){
            Picasso.with( this.inflater.getContext()).load( user.getProfile_picture_thumbnail() ).placeholder(R.drawable.no_profile_picture).into( holder.profilePicture );
        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatIntent = new Intent(holder.context , ChatActivity.class );
                chatIntent.putExtra("friendUserId", user.getKey());
                holder.context.startActivity(chatIntent);

/*                Notification newNotification = new Notification();
                newNotification.setTitle( "New chat message" );
                newNotification.setBody( "This is the description of my notification" );
                newNotification.setIcon( "default" );
                newNotification.setNotificationType( "chat" );
                newNotification.setUserReceiverKey( user.getKey() );
                newNotification.setClickAction( "ch.androidapp.hevs.fulltopia.Modules.Message.ChatActivity.NOTIFICATION" );
                DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference().child( "notifications" ).child( user.getKey() ).push();
                notificationReference.setValue( newNotification );*/

            }
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class PostsViewHolder extends RecyclerView.ViewHolder {
        View view;

        private Context context;
        TextView userName, messageDate,messageText ;
        CircleImageView profilePicture;
        String id;

        public PostsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.context = itemView.getContext();

        }
    }
}
