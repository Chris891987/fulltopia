package ch.androidapp.hevs.fulltopia.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mikepenz.iconics.IconicsDrawable;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureChild;
import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureParent;
import ch.androidapp.hevs.fulltopia.Modules.Community.CommunityViewActivity;
import ch.androidapp.hevs.fulltopia.Modules.Event.EventViewActivity;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jacques on 05.09.2017.
 */

public class RecyclerViewSearchChildAdapter extends RecyclerView.Adapter<RecyclerViewSearchChildAdapter.SingleItemRowHolder> {

    private Context mContext;
    private List<StructureChild> searchChildsResults;
    private StorageReference mStorage;


    public RecyclerViewSearchChildAdapter(Context mContext, List<StructureChild> searchChildsResults)  {
        this.mContext = mContext;
        this.searchChildsResults = searchChildsResults;
    }


    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.search_result_list_childs, null );
        SingleItemRowHolder viewHolder = new SingleItemRowHolder( view );

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, int position) {
        final StructureChild sc = searchChildsResults.get( position );

        holder.title.setText(sc.getTitle());
        holder.details.setText(sc.getDescription());
        holder.id=sc.getId();

        switch ( sc.getType().toString() ) {
            case "Event":
                holder.type = FulltopiaUtils.TYPE_EVENT;
                break;
            case "Community":
                holder.type = FulltopiaUtils.TYPE_COMMUNITY;
                break;
            case "User":
                holder.type = FulltopiaUtils.TYPE_USER;
                break;
        }

        //Recovering the image stored on firebase
        mStorage = FirebaseStorage.getInstance().getReference(sc.getImgPath() );
        mStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

        Glide.with(mContext /* context */)
                .load( uri.toString())
                .into(holder.thumbnail);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

                Glide.with(mContext /* context */)
                        .load( R.drawable.no_image )
                        .into(holder.thumbnail);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ( null != searchChildsResults ? searchChildsResults.size() : 0 );
    }

    public  class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected View view;
        protected TextView title ;
        protected TextView details;
        protected CircleImageView thumbnail ;
        protected String id;

        protected String type;


        public SingleItemRowHolder(View view) {
            super( view );

            this.title = ( TextView ) view.findViewById( R.id.textview_search_child_title );
            this.details = ( TextView ) view.findViewById( R.id.textview_search_child_details );
            this.thumbnail = ( CircleImageView ) view.findViewById( R.id.img_search_child_thumbnail );

            view.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick( View v ) {
                    // TODO What about the putExtra ?!? How to know which object 'll be opening ?
                    if(type.equals(FulltopiaUtils.TYPE_COMMUNITY)){
                        Intent intent = new Intent(mContext, CommunityViewActivity.class);
                        intent.putExtra("id",id);
                        mContext.startActivity(intent);
                    }
                    else if (type.equals(FulltopiaUtils.TYPE_EVENT)){
                        Intent intent = new Intent(mContext, EventViewActivity.class);
                        intent.putExtra("id",id);
                        mContext.startActivity(intent);
                    }
                    //TODO Open a new message with the selected contact
                    /*else if (type.equals(FulltopiaUtils.TYPE_USER)){
                        Intent intent = new Intent(mContext, EventViewActivity.class);
                        mContext.startActivity(intent);
                    }*/
                }
            });

        }

    }
}
