package ch.androidapp.hevs.fulltopia.Entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by Christian on 14.07.2017.
 */
@IgnoreExtraProperties
public abstract class AbstractObject {

    @Exclude
    protected String id;
    protected String name;
    protected String description;
    protected String createdAt;
    protected String updateAt;
    protected Double locationLatitude;
    protected Double locationLongitude;
    protected String locationName;
    protected HashMap<String, Boolean> categories;
    protected HashMap<String, Boolean> usersJoined;
    protected String owner;
    protected Boolean urlDefault;

    public AbstractObject(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public String getName() {
        return name;
    }

    public AbstractObject setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AbstractObject setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public AbstractObject setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public AbstractObject setUpdateAt(String upStringAt) {
        this.updateAt = upStringAt;
        return this;
    }

    public String getOwner() {
        return owner;
    }

    public AbstractObject setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public String getId() {
        return id;
    }

    public AbstractObject setId(String id) {
        this.id = id;
        return this;
    }

    public HashMap<String, Boolean> getCategories() {
        return categories;
    }

    public AbstractObject setCategories(HashMap<String, Boolean> categories) {
        this.categories = categories;
        return this;
    }

    public HashMap<String, Boolean> getUsersJoined() {
        return usersJoined;
    }

    public AbstractObject setUsersJoined(HashMap<String, Boolean> usersJoined) {
        this.usersJoined = usersJoined;
        return this;
    }

    public Boolean getUrlDefault() {
        return urlDefault;
    }

    public AbstractObject setUrlDefault(Boolean urlDefault) {
        this.urlDefault = urlDefault;
        return this;
    }

    public Double getLocationLatitude() {
        return locationLatitude;
    }

    public AbstractObject setLocationLatitude(Double locationLatitude) {
        this.locationLatitude = locationLatitude;
        return this;
    }

    public Double getLocationLongitude() {
        return locationLongitude;
    }

    public AbstractObject setLocationLongitude(Double locationLongitude) {
        this.locationLongitude = locationLongitude;
        return this;
    }

    public String getLocationName() {
        return locationName;
    }

    public AbstractObject setLocationName(String locationName) {
        this.locationName = locationName;
        return this;
    }

    @Override
    public String toString() {
        return "AbstractObject{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updateAt='" + updateAt + '\'' +
                ", locationLatitude='" + locationLatitude + '\'' +
                ", locationLongitude='" + locationLongitude + '\'' +
                ", locationName='" + locationName + '\'' +
                ", categories=" + categories +
                ", usersJoined=" + usersJoined +
                ", owner='" + owner + '\'' +
                ", urlDefault=" + urlDefault +
                '}';
    }
}
