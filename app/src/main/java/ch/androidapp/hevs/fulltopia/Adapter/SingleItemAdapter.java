package ch.androidapp.hevs.fulltopia.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Christian-Lenovo on 29.08.2017.
 */

public class SingleItemAdapter extends RecyclerView.Adapter<SingleItemAdapter.ItemsViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<String> items = new ArrayList<>();

    public SingleItemAdapter(LayoutInflater inflater, HashMap<String,Boolean> itemsHash) {
        this.inflater = inflater;
        for ( String key : itemsHash.keySet() ) {
            this.items.add(key);
        }
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemPost = inflater.inflate(R.layout.list_simple_item, parent, false);
        ItemsViewHolder postsViewHolder = new ItemsViewHolder( itemPost);
        postsViewHolder.view = itemPost;
        postsViewHolder.text = (TextView) itemPost.findViewById(R.id.text);

        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemsViewHolder holder, final int position) {
        String val = items.get(position);
        DatabaseReference referenceDB = FirebaseDatabase.getInstance().getReference().child( "categories" ).child(val);
        referenceDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    if(childSnapshot.getKey().equals("name") ){
                        holder.text.setText(childSnapshot.getValue( String.class ));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        holder.position = position;

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ItemsViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView text;
        int position;

        public ItemsViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
        }
    }
}
