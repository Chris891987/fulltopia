package ch.androidapp.hevs.fulltopia.Entities;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Christian on 14.07.2017.
 */
@IgnoreExtraProperties
public class Event extends AbstractObject {
    
    private String startDate;
    private String endDate;
    private Integer maxPartecipants;
    private Integer minPartecipants;


    public Event(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public String getStartDate() {
        return startDate;
    }

    public Event setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public Event setEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public Integer getMaxPartecipants() {
        return maxPartecipants;
    }

    public Event setMaxPartecipants(Integer maxPartecipants) {
        this.maxPartecipants = maxPartecipants;
        return this;
    }

    public Integer getMinPartecipants() {
        return minPartecipants;
    }

    public Event setMinPartecipants(Integer minPartecipants) {
        this.minPartecipants = minPartecipants;
        return this;
    }



}
