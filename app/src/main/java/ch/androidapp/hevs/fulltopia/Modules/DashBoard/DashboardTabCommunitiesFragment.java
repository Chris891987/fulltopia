package ch.androidapp.hevs.fulltopia.Modules.DashBoard;

import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import ch.androidapp.hevs.fulltopia.Adapter.ItemDashboardAdapter;
import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewSearchAdapter;
import ch.androidapp.hevs.fulltopia.Adapter.StepperCategoriesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.Community;
import ch.androidapp.hevs.fulltopia.Entities.AbstractObject;
import ch.androidapp.hevs.fulltopia.Entities.Event;
import ch.androidapp.hevs.fulltopia.Entities.SectionDataModel;
import ch.androidapp.hevs.fulltopia.Interface.OnGetOnceFirebasedataListener;
import ch.androidapp.hevs.fulltopia.Modules.Search.SearchResultActivity;
import ch.androidapp.hevs.fulltopia.R;
import ch.androidapp.hevs.fulltopia.Services.FirebaseMistakes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DashboardTabCommunitiesFragment extends Fragment {

    private ArrayList<SectionDataModel> allData = new ArrayList<SectionDataModel>();
    private DatabaseReference favoritedUser;
    private FirebaseUser currentUser;
    private FirebaseAuth firebaseAuth;
    private Community community;
    private View myInflatedView;
    private ItemDashboardAdapter adapter;
    private ValueEventListener appreciatedCategories;
    private ValueEventListener categories;
    private ValueEventListener categoriesCommunities;
    private ValueEventListener communities;
    private RecyclerView my_recycler_view;
    private Multimap<String, Community> results =  ArrayListMultimap.create();
    private Set<String> listCategories = new HashSet<>();
    private int counter = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myInflatedView = inflater.inflate(R.layout.dashboard_fragment_tab_communities, container, false);
        allData = new ArrayList<SectionDataModel>();

        my_recycler_view = (RecyclerView) myInflatedView.findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);
        my_recycler_view.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        //Recover user
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            appreciatedCategories = new FirebaseMistakes().readDataOnce("users/" + currentUser.getUid() + "/appreciatedCategories", new OnGetOnceFirebasedataListener() {
                @Override
                public void onStart() {
                    counter++;
                }
                @Override
                public void onSuccess(DataSnapshot allFavoritedCategories) {
                    for (final DataSnapshot currentCategorie : allFavoritedCategories.getChildren()) {
                        categories = new FirebaseMistakes().readDataOnce("categories/" + currentCategorie.getKey(), new OnGetOnceFirebasedataListener() {
                            @Override
                            public void onStart() {
                                counter++;
                            }
                            @Override
                            public void onSuccess(final DataSnapshot currentCategory) {
                                categoriesCommunities = new FirebaseMistakes().readDataOnce("categories_communities/" + currentCategory.getKey(), new OnGetOnceFirebasedataListener() {
                                    @Override
                                    public void onStart() {
                                        counter++;
                                    }
                                    @Override
                                    public void onSuccess(DataSnapshot categoriesData) {
                                        for (final DataSnapshot communityData : categoriesData.getChildren()) {
                                            //Recover community from key
                                            communities = new FirebaseMistakes().readDataOnce("communities/" + communityData.getKey(), new OnGetOnceFirebasedataListener() {
                                                @Override
                                                public void onStart() {
                                                    counter++;
                                                }
                                                @Override
                                                public void onSuccess(DataSnapshot data) {
                                                    community = data.getValue(Community.class);
                                                    community.setId(data.getKey());
                                                    String name = currentCategory.child("name").getValue(String.class);
                                                    listCategories.add(name);
                                                    results.put(name,community);
                                                    checkIsOver();
                                                }
                                                @Override
                                                public void onFailed(DatabaseError databaseError) {
                                                    checkIsOver();
                                                }
                                            });
                                        }
                                        checkIsOver();
                                    }
                                    @Override
                                    public void onFailed(DatabaseError databaseError) {
                                        checkIsOver();
                                    }
                                });
                                checkIsOver();
                            }
                            @Override
                            public void onFailed(DatabaseError databaseError) {
                                checkIsOver();
                            }
                        });
                    }
                    checkIsOver();
                }
                @Override
                public void onFailed(DatabaseError databaseError) {
                    checkIsOver();
                }

                public void checkIsOver() {
                    counter--;
                    if (counter == 0) {
                        for (String cat : listCategories){
                            SectionDataModel dm = new SectionDataModel();
                            dm.setHeaderTitle(cat);
                            Collection<Community> comm = results.get(cat);
                            ArrayList<AbstractObject> singleAbstractObject = new ArrayList<AbstractObject>();
                            for(AbstractObject c : comm)
                                singleAbstractObject.add(c);
                            dm.setAllItemsInSection(singleAbstractObject);
                            allData.add(dm);
                        }
                        adapter = new ItemDashboardAdapter(getContext(), allData);
                        my_recycler_view.setAdapter(adapter);
                        FirebaseDatabase.getInstance().getReference().removeEventListener(appreciatedCategories);
                        FirebaseDatabase.getInstance().getReference().removeEventListener(categories);
                        FirebaseDatabase.getInstance().getReference().removeEventListener(categoriesCommunities);
                        FirebaseDatabase.getInstance().getReference().removeEventListener(communities);
                    }
                }
            });
        }
        return myInflatedView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.addcommunity, menu);
        return;
    }


}
