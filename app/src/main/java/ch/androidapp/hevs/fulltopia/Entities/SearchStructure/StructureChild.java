package ch.androidapp.hevs.fulltopia.Entities.SearchStructure;

/**
 * Created by Jacques on 07.09.2017.
 */

public class StructureChild {
    private String imgPath = "";
    private String title = "";
    private String description = "";
    private String type;
    private String id;

    public StructureChild() {
    }

    public StructureChild(String imgPath, String title, String description) {
        this.imgPath = imgPath;
        this.title = title;
        this.description = description;

    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "StructureChild{" +
                "imgPath='" + imgPath + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
