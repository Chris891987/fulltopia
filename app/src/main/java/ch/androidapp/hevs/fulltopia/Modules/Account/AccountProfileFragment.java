package ch.androidapp.hevs.fulltopia.Modules.Account;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ch.androidapp.hevs.fulltopia.Entities.User;
import ch.androidapp.hevs.fulltopia.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

/**
 * User profile page
 */
public class AccountProfileFragment extends Fragment {

    private static AccountProfileFragment fragment = null;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private DatabaseReference referenceCurrentUser;

    private User user;

    private TextView labelDisplayName;
    private TextView labelDisplayStatus;
    private TextView labelDisplayEmail;
    private CircleImageView profilePictureView;
    private ImageView profilePictureView2;



    public static AccountProfileFragment newInstance() {
        fragment = new AccountProfileFragment();
        return fragment;
    }


    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        View v = inflater.inflate( R.layout.account_profile_fragment, container, false);
        labelDisplayName = (TextView) v.findViewById( R.id.account_profile_name_label);
        labelDisplayStatus = (TextView) v.findViewById( R.id.account_profile_status_label);
        labelDisplayEmail = (TextView) v.findViewById( R.id.account_profile_email_label);
        profilePictureView2 = (ImageView) v.findViewById( R.id.profile_picture );



        //connection to firebase + get user current
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        String currentUserId = currentUser.getUid();
        final String userEmail = currentUser.getEmail();

        //displaying data (from the current user)from the database on the page
        referenceCurrentUser = FirebaseDatabase.getInstance().getReference().child("users").child( currentUserId );
        referenceCurrentUser.getRef().addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //user = dataSnapshot.getValue(User.class);
                //System.out.println(user.getUsername());

                labelDisplayName.setText(dataSnapshot.child("name").getValue(String.class));
                labelDisplayStatus.setText(dataSnapshot.child("status").getValue(String.class));
                labelDisplayEmail.setText(userEmail);


                String picturePath = dataSnapshot.child("profile_picture").getValue(String.class);

                //PICASSO - image managment
                if(!picturePath.equals("default")){
                    Picasso.with(getContext()).load(picturePath).placeholder(R.drawable.no_picture).into(profilePictureView2);
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });


        return v;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    //profile menu added to the toolBar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        inflater.inflate(R.menu.profile, menu);
        return;
    }







}
