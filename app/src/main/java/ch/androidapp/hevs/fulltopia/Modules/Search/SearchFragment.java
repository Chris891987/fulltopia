package ch.androidapp.hevs.fulltopia.Modules.Search;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ch.androidapp.hevs.fulltopia.Adapter.StepperCategoriesAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Modules.DashBoard.DashboardFragment;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;

import ch.androidapp.hevs.fulltopia.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Search
 */
public class SearchFragment extends Fragment {

    private ArrayList<Category> allData = new ArrayList<Category>();
    View rootView;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private static SearchFragment fragment = null;
    private DatabaseReference favoritedSearchCurrentUser;

    public static SearchFragment newInstance() {
        fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.search_fragment, container, false);
        FulltopiaUtils.setContext(getContext());

        //firebase connection
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        String currentUserId = currentUser.getUid();

        //set data searchable
        allData = FulltopiaUtils.getSearchableCategories();

        //Fill categories
        this.favoritedSearchCurrentUser = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
        this.favoritedSearchCurrentUser.child("favoritedSearch").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot allFavoritedSearch) {
                System.out.println("JACK - " + allFavoritedSearch.exists());
                for (DataSnapshot currentFavoritedSearch : allFavoritedSearch.getChildren()) {
                    for (Category currentCategoryInList : allData) {
                        if (currentCategoryInList.getKey().toString().equals(currentFavoritedSearch.getKey())) {
                            currentCategoryInList.setChecked(true);
                        }
                    }
                }

                StepperCategoriesAdapter adapter = new StepperCategoriesAdapter(getContext(), allData);
                RecyclerView categoriesRecyclerView = (RecyclerView) rootView.findViewById(R.id.searchable_entities_recyclerView);
                categoriesRecyclerView.setHasFixedSize(true);
                categoriesRecyclerView.setAdapter(adapter);

                // Manage if portrait or landscape
                int col;
                if (getActivity().getResources().getConfiguration().orientation == 1)
                    col = 2; // Portrait > 2 colums
                else
                    col = 3;// landscape > 3 colums

                categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), col));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        ComponentName cn = new ComponentName(getContext(), SearchResultActivity.class);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(getActivity().getBaseContext(), "QUERY : " + query, Toast.LENGTH_LONG).show();
                Intent startIntent = new Intent(getContext(), SearchResultActivity.class);
                startIntent.putExtra("myQuery", query);
                startActivity(startIntent);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                System.out.println("NEW : " + newText);
                return false;
            }
        });

        return;
    }


}
