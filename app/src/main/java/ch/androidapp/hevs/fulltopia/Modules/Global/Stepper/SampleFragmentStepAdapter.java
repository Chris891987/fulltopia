package ch.androidapp.hevs.fulltopia.Modules.Global.Stepper;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupCategoryFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupLocalisationFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupPhotoFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupSharingFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupSubCategoryFragment;
import ch.androidapp.hevs.fulltopia.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class SampleFragmentStepAdapter extends AbstractFragmentStepAdapter  implements OnMapReadyCallback {

    public SampleFragmentStepAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context);
        switch (position) {
            case 0:
                builder.setNextButtonLabel(R.string.location)
                        .setBackButtonLabel(R.string.logout)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE);
                break;
            case 1:
                builder.setNextButtonLabel(R.string.categories)
                        .setBackButtonLabel(R.string.stepper_profile_change_status_title)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;
            case 2:
                builder.setNextButtonLabel(R.string.complete)
                        .setBackButtonLabel(R.string.location)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;
/*            case 3:
                builder.setNextButtonLabel("My Sharing")
                        .setBackButtonLabel("Categories")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;
            case 4:
                builder.setNextButtonLabel("Complete Setup")
                        .setBackButtonLabel("Sub-Categories")
                        .setNextButtonEndDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;*/
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }

    //the 3 steps creation
    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                return new AccountSetupPhotoFragment();
            case 1:
                return new AccountSetupLocalisationFragment();
            case 2:
                return new AccountSetupCategoryFragment();
/*            case 3:
                return new AccountSetupSubCategoryFragment();
            case 4:
                return new AccountSetupSharingFragment();*/
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public void onMapReady( GoogleMap googleMap ) {

    }
}