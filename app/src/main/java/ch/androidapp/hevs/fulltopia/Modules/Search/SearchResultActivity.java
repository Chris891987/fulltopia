package ch.androidapp.hevs.fulltopia.Modules.Search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ch.androidapp.hevs.fulltopia.Adapter.RecyclerViewSearchAdapter;
import ch.androidapp.hevs.fulltopia.Entities.Category;
import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureChild;
import ch.androidapp.hevs.fulltopia.Entities.SearchStructure.StructureParent;
import ch.androidapp.hevs.fulltopia.Modules.Global.FulltopiaUtils;
import ch.androidapp.hevs.fulltopia.R;

public class SearchResultActivity extends AppCompatActivity {
    private ArrayList<Category> allData;
    private ArrayList<Category> selectedData;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private DatabaseReference favoritedSearchCurrentUser;
    private DatabaseReference foundObject;


    private long maxState;
    private long currentState;

    private RecyclerViewSearchAdapter searchAdapter;
    //private StructureParent sp = new StructureParent();

    private List<StructureParent> results = new ArrayList<StructureParent>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_activity);
        FulltopiaUtils.setContext(this);
        final String query = getIntent().getExtras().getString("myQuery");

        //Remplir categories
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();

        //selectedData = new ArrayList<Category>();
        allData = FulltopiaUtils.getSearchableCategories();


        this.favoritedSearchCurrentUser = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
        this.favoritedSearchCurrentUser.child("favoritedSearch").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot allFavoritedSearch) {
                maxState = allFavoritedSearch.getChildrenCount();
                currentState = 0;
                for (DataSnapshot currentFavoritedSearch : allFavoritedSearch.getChildren()) {
                    for (Category currentCategoryInList : allData) {
                        if (currentCategoryInList.getKey().toString().equals(currentFavoritedSearch.getKey())) {
                            //selectedData.add(currentCategoryInList);
                            Log.w("JACK --- QUERY ",query);
                            switch (currentCategoryInList.getKey().toString()) {
                                case "100":  // Community
                                    //sp.setName("Communities");

                                    final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("communities");
                                    ref.orderByChild("name").startAt(query).endAt(query+"\uf8ff").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Log.w("JACK : " , "BEGIN --- Communities");
                                            StructureParent sp = new StructureParent();
                                            sp.setName("Communities");
                                            sp.setBackgroundColor("#f39c12");

                                            for (DataSnapshot d : dataSnapshot.getChildren()) {

                                                final StructureChild newChild = new StructureChild();
                                                newChild.setTitle(d.child("name").getValue(String.class));
                                                newChild.setDescription(d.child("description").getValue(String.class));
                                                newChild.setId(d.getKey());
                                                newChild.setImgPath("pictures/communities/"+d.getKey());
                                                newChild.setType("Community");
                                                Log.w("JACK : " , newChild.toString());
                                                sp.addChild(newChild);
                                            }

                                            if (!(sp.getName().isEmpty())) {
                                                results.add(sp);
                                            }

                                            currentState++;
                                            printResults();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                                    break;
                                case "200":  // Events

                                    final DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference().child("events");
                                    ref2.orderByChild("name").startAt(query).endAt(query+"\uf8ff").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Log.w("JACK : ", "BEGIN --- Communities");
                                            StructureParent sp = new StructureParent();
                                            sp.setName("Events");
                                            sp.setBackgroundColor("#27ae60");

                                            for (DataSnapshot d : dataSnapshot.getChildren()) {

                                                final StructureChild newChild = new StructureChild();
                                                newChild.setTitle(d.child("name").getValue(String.class));
                                                newChild.setDescription(d.child("description").getValue(String.class));
                                                newChild.setId(d.getKey());
                                                newChild.setImgPath("pictures/events/"+d.getKey());
                                                newChild.setType("Event");

                                                //newChild.setImgPath(d.child("imgPath").getValue(String.class));

                                                Log.w("JACK : ", newChild.toString());
                                                sp.addChild(newChild);

                                            }

                                            if (!(sp.getName().isEmpty())) {
                                                results.add(sp);
                                            }

                                            currentState++;
                                            printResults();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }

                                    });

                                    break;
                                case "400":  // People
                                    final DatabaseReference ref3 = FirebaseDatabase.getInstance().getReference().child("users");
                                    ref3.orderByChild("name").startAt(query).endAt(query+"\uf8ff").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Log.w("JACK : ", "BEGIN --- Communities");
                                            StructureParent sp = new StructureParent();
                                            sp.setName("People");
                                            sp.setBackgroundColor("#3498db");;

                                            for (DataSnapshot d : dataSnapshot.getChildren()) {

                                                final StructureChild newChild = new StructureChild();
                                                newChild.setTitle(d.child("name").getValue(String.class));
                                                newChild.setDescription(d.child("status").getValue(String.class));
                                                newChild.setId(d.getKey());
                                                newChild.setImgPath("pictures/profiles/thumbs/"+d.getKey()+".jpg");
                                                newChild.setType("User");

                                                Log.w("JACK : ", newChild.toString());
                                                sp.addChild(newChild);

                                            }

                                            if (!(sp.getName().isEmpty())) {
                                                results.add(sp);
                                            }

                                            currentState++;
                                            printResults();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }


                                    });
                                    break;

                            }
                        }
                    }
                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //Toast.makeText(getBaseContext(), "handle Intent > QUERY : " + query, Toast.LENGTH_LONG).show();
        }
    }


    private void printResults (){

        if(currentState==maxState) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_result);

            searchAdapter = new RecyclerViewSearchAdapter(SearchResultActivity.this, results);

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(searchAdapter);
        }
    }
}
