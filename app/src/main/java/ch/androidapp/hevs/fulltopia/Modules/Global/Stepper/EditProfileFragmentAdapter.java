package ch.androidapp.hevs.fulltopia.Modules.Global.Stepper;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupCategoryFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupLocalisationFragment;
import ch.androidapp.hevs.fulltopia.Modules.Account.AccountSetupPhotoFragment;
import ch.androidapp.hevs.fulltopia.R;

/**
 * The stepper for edition profile
 */
public class EditProfileFragmentAdapter extends SampleFragmentStepAdapter {

    public EditProfileFragmentAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    //The 3 steps of the edition profile
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context);
        switch (position) {
            case 0:
                builder.setNextButtonLabel(R.string.location)
                        .setBackButtonLabel(R.string.back)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE);
                break;
            case 1:
                builder.setNextButtonLabel(R.string.categories)
                        .setBackButtonLabel(R.string.stepper_profile_change_status_title)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;
            case 2:
                builder.setNextButtonLabel(R.string.finish)
                        .setBackButtonLabel(R.string.location)
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_left);
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}